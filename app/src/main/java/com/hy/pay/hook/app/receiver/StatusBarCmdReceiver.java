package com.hy.pay.hook.app.receiver;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;

import com.hy.pay.hook.app.AppLogger;
import com.hy.pay.hook.app.service.NotificationService;

public class StatusBarCmdReceiver extends BroadcastReceiver {

    private static final String TAG = StatusBarCmdReceiver.class.getSimpleName();

    private NotificationService service;
    public StatusBarCmdReceiver(NotificationService service) {
        this.service = service;
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        StatusBarNotification[] sbns = service.getActiveNotifications();
        if (null == sbns) {
            AppLogger.i(TAG, "获取到0个通知栏消息");
            return;
        }
        AppLogger.i(TAG, "获取到"+ sbns.length +"个通知栏消息");
        for (StatusBarNotification sbn:sbns) {
            Notification notification = sbn.getNotification();
            Bundle extras = notification.extras;

            String title = extras.getString(Notification.EXTRA_TITLE);
            String content = extras.getString(Notification.EXTRA_TEXT);

            AppLogger.i(TAG, "获取通知栏消息->标题："+ title +"，内容： " + content);
        }
    }
}
