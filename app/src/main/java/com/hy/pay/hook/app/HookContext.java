package com.hy.pay.hook.app;

import android.app.AndroidAppHelper;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public class HookContext {

    public static Context getContext() {
        return AndroidAppHelper.currentApplication();
    }

    public static void sendBroadcast(Intent intent) {
        HookContext.getContext().sendBroadcast(intent);
    }

    public static void registerReceiver(BroadcastReceiver receiver, IntentFilter filter) {
        HookContext.getContext().registerReceiver(receiver, filter);
    }
}
