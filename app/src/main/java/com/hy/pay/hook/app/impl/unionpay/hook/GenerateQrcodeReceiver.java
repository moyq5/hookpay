package com.hy.pay.hook.app.impl.unionpay.hook;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.hy.pay.hook.app.AppContext;
import com.hy.pay.hook.app.HookLogger;
import com.hy.pay.hook.app.impl.unionpay.GeneratedQrcodeReceiver;

import org.json.JSONObject;

public class GenerateQrcodeReceiver extends BroadcastReceiver {
    private static final String TAG = GenerateQrcodeReceiver.class.getSimpleName();
    @Override
    public void onReceive(Context context, Intent intent) {

        Unionpayer.getInstance().reset();

        final String orderNo = intent.getStringExtra("orderNo");
        final String payType = intent.getStringExtra("payType");
        final String amount = intent.getStringExtra("amount");
        final String payAmount = intent.getStringExtra("payAmount");
        final String remark = intent.getStringExtra("remark");
        new Thread(() -> {
            String json = HookUtils.generateQrcode(payAmount, remark);
            if (null == json || json.isEmpty()) {
                HookLogger.w(TAG, "生成收款码信息：null");
                return;
            }
            JSONObject jsonObj = null;
            try {
                jsonObj = new JSONObject(json);
                String resp = jsonObj.getString("resp");
                if (null == resp || !resp.equals("00")) {
                    HookLogger.e(TAG, "获取收款码失败：" + jsonObj.getString("msg"));
                    return;
                }
                JSONObject params = jsonObj.getJSONObject("params");
                String qrcode = params.getString("certificate");
                //String channelOrderNo = params.getString("orderId");

                Intent generatedReceiverIntent = new Intent(GeneratedQrcodeReceiver.class.getName());
                generatedReceiverIntent.putExtra("orderNo", orderNo);
                generatedReceiverIntent.putExtra("payType", payType);
                generatedReceiverIntent.putExtra("amount", amount);
                generatedReceiverIntent.putExtra("channelOrderNo", orderNo);
                generatedReceiverIntent.putExtra("payAmount", payAmount);
                generatedReceiverIntent.putExtra("qrcode", qrcode);
                AppContext.sendBroadcast(generatedReceiverIntent);

            } catch (Exception e) {
                HookLogger.e(TAG, e);
            }
        }).start();
    }
}
