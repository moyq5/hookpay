package com.hy.pay.hook.app.impl.quanfutong.hook;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.hy.pay.hook.app.HookLogger;

import java.lang.reflect.Field;
import java.util.List;

public class CheckOrderReceiver extends BroadcastReceiver {
    private static final String TAG = CheckOrderReceiver.class.getSimpleName();
    @Override
    public void onReceive(Context context, Intent intent) {
        new Thread(() -> check()).start();

    }

    private void check() {
        QuanfutongHook.getInstance().refreshOrderList();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {}

        List<Object> list = QuanfutongHook.getInstance().orderList();
        HookLogger.i(TAG, "size: " + list.size());
        for (Object obj: list) {
            HookLogger.i(TAG, "#######################################");
            Field[] fields = obj.getClass().getDeclaredFields();
            for (Field field: fields) {
                field.setAccessible(true);
                HookLogger.i(TAG, "name: " + field.getName());
                HookLogger.i(TAG, "type: " + field.getType());
                try {
                    HookLogger.i(TAG, "value: " + field.get(obj));
                } catch (IllegalAccessException e) {}
            }
            // tradeState = 2
            // tradeStateText = "收款成功"
            // tradeTime = "2019-06-21 12:02:09"
            // transactionId = "420000030520190621029964595" // 渠道单号
            // transactionType = 0
            // money = 10 long
            // orderNoMch = "102576468263201906215106843620" // 交易单号
            // outTradeNo = "10257646826367858291128773204" // 客户单号
            // pageCount = 0 int
        }
    }

}
