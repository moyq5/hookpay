package com.hy.pay.hook.app.impl.unionpay;

import android.content.Intent;
import android.content.IntentFilter;

import com.hy.pay.hook.app.AppLogger;
import com.hy.pay.hook.app.MainAbstract;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrder;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrderDao;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrderDaoFactory;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrderDaoImpl;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrderDbHelper;
import com.hy.pay.hook.app.impl.unionpay.hook.CheckOrderReceiver;
import com.hy.pay.hook.app.HookContext;
import com.hy.pay.hook.app.iot.device.Device;

import java.util.List;

public class UnionpayMain extends MainAbstract {
    private static final String TAG = UnionpayMain.class.getSimpleName();

    private GeneratedQrcodeReceiver genertatedReceiver;

    private CheckedOrderReceiver checkedReceiver;

    private Thread checkOrderService;

    public UnionpayMain(Device device) {
        super(device);
        genertatedReceiver = new GeneratedQrcodeReceiver(device);
        checkedReceiver = new CheckedOrderReceiver();
    }

    @Override
    protected void createInit() {
        device.action = new UnionpayDeviceAction(device);

        device.context.registerReceiver(genertatedReceiver,
                new IntentFilter(GeneratedQrcodeReceiver.class.getName()));
        device.context.registerReceiver(checkedReceiver,
                new IntentFilter(CheckedOrderReceiver.class.getName()));

        MoneyOrderDbHelper helper = new MoneyOrderDbHelper(device.context, device.deviceName + ".db", null, 1);
        MoneyOrderDao dao = new MoneyOrderDaoImpl(helper, 300, 3000);// TODO
        MoneyOrderDaoFactory.setDao(dao);

        checkOrderService = startCheckService();
    }

    @Override
    public void destroy() {
        checkOrderService.interrupt();
        try {
            device.context.unregisterReceiver(genertatedReceiver);
            device.context.unregisterReceiver(checkedReceiver);
        } catch (IllegalArgumentException e) {
            if (!e.getMessage().contains("Receiver not registered")) {
                throw e;
            }
        }
        super.destroy();
    }

    private Thread startCheckService() {
        Thread thread = new Thread(() -> {
            Intent intent = new Intent(CheckOrderReceiver.class.getName());
            try {
                while(true) {
                    MoneyOrderDao dao = MoneyOrderDaoFactory.getDao();
                    List<MoneyOrder> list = dao.queryUnpaied(System.currentTimeMillis() - 300000);// TODO 5分钟内的订单
                    AppLogger.i(TAG,"最近有" + list.size() + "条未支付订单");

                    if(list.size() > 0) {
                        HookContext.sendBroadcast(intent);
                    }

                    Thread.sleep(30000);
                }
            } catch (InterruptedException e) {
                AppLogger.v(TAG, "", e);
            } catch (Exception e) {
                AppLogger.e(TAG, "", e);
            }

        });
        thread.start();
        return thread;
    }

}
