package com.hy.pay.hook.app.iot.device.data;

public class QrcodeData {
    public String channelOrderNo;
    public String orderNo;
    public String payType;
    public String amount;
    public String payAmount;
    public String qrcode;
}
