package com.hy.pay.hook.app;

public interface Constants {
    String WIFTPASS_QRCODE_URL_FORMAT = "https://spay3.swiftpass.cn/spay/merchant/scanQr?qrId=%s&fixMoney=%s";
    String TOPIC_ORDER_SUFFIX = "/user/app/pay/order";
    String TOPIC_QRCODE_SUFFIX = "/user/app/pay/qrcode";
    String TOPIC_NOTIFY_SUFFIX = "/user/app/pay/notify";
    String TOPIC_NOTIFY_REPLY_SUFFIX = "/user/app/pay/notify_reply";
    String TOPIC_ORDER_FORMAT = "/%s/%s" + TOPIC_ORDER_SUFFIX;
    String TOPIC_QRCODE_FORMAT = "/%s/%s" + TOPIC_QRCODE_SUFFIX;
    String TOPIC_NOTIFY_FORMAT = "/%s/%s" + TOPIC_NOTIFY_SUFFIX;
    String TOPIC_NOTIFY_REPLY_FORMAT = "/%s/%s" + TOPIC_NOTIFY_REPLY_SUFFIX;

}
