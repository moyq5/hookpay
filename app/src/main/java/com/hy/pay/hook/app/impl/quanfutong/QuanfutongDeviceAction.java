package com.hy.pay.hook.app.impl.quanfutong;

import android.content.Context;
import android.widget.Toast;

import com.hy.pay.hook.app.Constants;
import com.hy.pay.hook.app.AppLogger;
import com.hy.pay.hook.app.iot.device.DeviceActionAbstract;
import com.hy.pay.hook.app.iot.device.Device;
import com.hy.pay.hook.app.iot.device.data.NotifyReplyData;
import com.hy.pay.hook.app.iot.device.data.OrderData;
import com.hy.pay.hook.app.iot.device.data.QrcodeData;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrder;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrderDao;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrderDaoFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class QuanfutongDeviceAction extends DeviceActionAbstract {
    private static final String TAG = QuanfutongDeviceAction.class.getSimpleName();
    private static final String ACTION_NAME = "全付通->";

    private Context context;

    public QuanfutongDeviceAction(Context context, Device device) {
        super(device);
        this.context = context;
    }

    @Override
    public void receiveOrder(OrderData data) {
        MoneyOrderDao dao = MoneyOrderDaoFactory.getDao();
        MoneyOrder order = dao.create(data.orderNo, Integer.parseInt(data.payType), data.amount);
        if (null == order) {
            Toast toast=Toast.makeText(context, "指定金额["+ data.amount +"]无法生成订单", Toast.LENGTH_SHORT);
            toast.show();
            AppLogger.w(TAG,ACTION_NAME + "指定金额["+ data.amount +"]无法生成订单");
            return;
        }

        final String qrcodeAmount = new BigDecimal(order.getPayAmount()).
                multiply(new BigDecimal("100")).
                setScale(0, RoundingMode.DOWN).
                toPlainString();

        final QrcodeData qrcodeData = new QrcodeData();
        qrcodeData.amount = data.amount;
        qrcodeData.channelOrderNo = data.orderNo;
        qrcodeData.orderNo = data.orderNo;
        qrcodeData.payType = data.payType;
        qrcodeData.payAmount = order.getPayAmount();
        qrcodeData.qrcode = String.format(Constants.WIFTPASS_QRCODE_URL_FORMAT, data.transCode, qrcodeAmount);
        sendQrcode(qrcodeData);

    }

    @Override
    public void receiveNotifyReply(NotifyReplyData data) {
        MoneyOrderDao dao = MoneyOrderDaoFactory.getDao();
        dao.remove(data.channelOrderNo, data.orderNo);
    }

}
