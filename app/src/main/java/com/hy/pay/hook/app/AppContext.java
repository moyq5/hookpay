package com.hy.pay.hook.app;

import android.app.AndroidAppHelper;
import android.content.Context;
import android.content.Intent;

public class AppContext {

    public static Context getContext() {
        return AndroidAppHelper.currentApplication();
    }

    public static void sendBroadcast(Intent intent) {
        AppContext.getContext().sendBroadcast(intent);
    }

}
