package com.hy.pay.hook.app.impl.sqlite.code;

import java.util.List;

/**
 * @author Moyq5
 * @date 2019年3月24日
 */
public interface CodeOrderDao {

	/**
	 * 添加订单信息，单号不可重复
	 * @param order
	 * @return
	 */
	CodeOrder addOrder(CodeOrder order);

	/**
	 * 根据平台单号更新订单支付状态
	 * @param orderNo
	 * @param payStatus
	 */
	void updatePayStatus(String orderNo, Integer payStatus);

	/**
	 * 根据平台单号更新订单通知状态为“通知成功”
	 * @param orderNo
	 * @return
	 */
	int updateNotifyStatus(String orderNo);

	/**
	 * 删除订单
	 * @param channelOrderNo 渠道单号
	 * @param orderNo 平台单号
	 * @return
	 */
	int remove(String channelOrderNo, String orderNo);

	/**
	 * 查询最近未付款订单
	 * @param afterTime 查此时间后的订单
	 * @return
	 */
	List<CodeOrder> queryUnpaied(Long afterTime);

	/**
	 * 查询未通知成功的订单
	 * @param beforeTime 上次通知时间在此之前的
	 * @param maxCount 通知次数少于该值的
	 * @return
	 */
	List<CodeOrder> queryUnnotified(Long beforeTime, Integer maxCount);

	/**
	 * 根据渠道单号获取订单
	 * @param channelOrderNo
	 * @return
	 */
	CodeOrder getOneByChannelOrderNo(String channelOrderNo);

	/**
	 * 更新订单通知次数（累计+1）
	 * @param id
	 */
	void updateNotifyTime(String channelOrderNo, String orderNo);


}
