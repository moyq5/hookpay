package com.hy.pay.hook.app.iot.device;

import com.hy.pay.hook.app.iot.device.data.NotifyData;
import com.hy.pay.hook.app.iot.device.data.NotifyReplyData;
import com.hy.pay.hook.app.iot.device.data.OrderData;
import com.hy.pay.hook.app.iot.device.data.QrcodeData;


public class DeviceActionExample extends DeviceActionAbstract {

    public DeviceActionExample(Device device) {
        super(device);
    }

    @Override
    public void receiveOrder(OrderData data) {
        final QrcodeData qrcodeData = new QrcodeData();
        qrcodeData.amount = data.amount;
        qrcodeData.channelOrderNo = data.orderNo;
        qrcodeData.orderNo = data.orderNo;
        qrcodeData.payAmount = data.amount;
        qrcodeData.payType = data.payType;
        qrcodeData.qrcode = "http://localhost/xxx";
        sendQrcode(qrcodeData);

        new Thread(() -> {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            NotifyData notifyData = new NotifyData();
            notifyData.amount = qrcodeData.amount;
            notifyData.channelOrderNo = qrcodeData.channelOrderNo;
            notifyData.orderNo = qrcodeData.orderNo;
            notifyData.payAmount = qrcodeData.payAmount;
            notifyData.payType = qrcodeData.payType;
            notifyData.status = "1";
            sendNotify(notifyData);
        }).start();
    }

    @Override
    public void receiveNotifyReply(NotifyReplyData data) {

    }

}
