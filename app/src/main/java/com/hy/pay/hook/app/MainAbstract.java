package com.hy.pay.hook.app;

import android.content.IntentFilter;

import com.hy.pay.hook.app.iot.device.DeviceService;
import com.hy.pay.hook.app.iot.device.Device;
import com.hy.pay.hook.app.iot.device.DeviceStartedException;
import com.hy.pay.hook.app.iot.device.LinkKitServer;
import com.hy.pay.hook.app.receiver.PayStatusReceiver;

public abstract class MainAbstract implements Main {

    private PayStatusReceiver payStatusReceiver;
    private DeviceService service;
    protected Device device;
    public MainAbstract(Device device) {
        this.device = device;
    }

    @Override
    public final void create() {


        createInit();

        payStatusReceiver = new PayStatusReceiver(device.action);

        device.context.registerReceiver(payStatusReceiver, new IntentFilter(PayStatusReceiver.class.getName()));

        service = LinkKitServer.getInstance();

        try {
            service.start(device);
        } catch (DeviceStartedException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void destroy() {
        service.stop();
        try {
            if (null != payStatusReceiver) {
                device.context.unregisterReceiver(payStatusReceiver);
            }

        } catch (IllegalArgumentException e) {
            if (e.getMessage().contains("Receiver not registered")) {
            } else {
                throw e;
            }
        }

    }

    protected abstract void createInit();
}
