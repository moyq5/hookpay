package com.hy.pay.hook.app;

import android.content.Context;
import android.os.Bundle;


import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

public class AppHook implements IXposedHookLoadPackage {
    private static Boolean inited = false;
    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam pkparam) throws Throwable {
        String packageName = pkparam.packageName;
        XposedBridge.log("加载到包：" + packageName);
        if (!"com.hy.pay.init.app".equals(packageName)) {
            return;
        }

        if (inited) {
            return;
        }
        inited = true;

        Object[] objArr = new Object[2];
        objArr[0] = Bundle.class;
        objArr[1] = new XC_MethodHook() {
            protected void afterHookedMethod(MethodHookParam mhparam)
                    throws Throwable {
                hookContext((Context) mhparam.thisObject);
            }
        };
        XposedHelpers.findAndHookMethod(MainActivity.class, "onCreate", objArr);
    }

    private void hookContext(Context context) {
        Object[] objArr = new Object[1];
        objArr[0] = new XC_MethodHook() {
            protected void afterHookedMethod(MethodHookParam mhparam)
                    throws Throwable {
                XposedBridge.log("init app Context：" + context);
                mhparam.setResult(context);
            }
        };
        XposedHelpers.findAndHookMethod(AppContext.class, "getContext", objArr);
    }

}
