package com.hy.pay.hook.app.iot.device;

import android.content.Intent;

import com.aliyun.alink.linksdk.cmp.core.base.AMessage;
import com.aliyun.alink.linksdk.cmp.core.base.ConnectState;
import com.aliyun.alink.linksdk.cmp.core.listener.IConnectNotifyListener;
import com.hy.pay.hook.app.AppLogger;
import com.hy.pay.hook.app.Constants;
import com.hy.pay.hook.app.iot.device.data.NotifyReplyData;
import com.hy.pay.hook.app.iot.device.data.OrderData;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ConnectNotifyListener implements IConnectNotifyListener {
    private static final String TAG = ConnectNotifyListener.class.getSimpleName();

    public static final String CONNECTED_ACTION = ConnectNotifyListener.class.getName() + ".CONNECTED";

    private Device device;
    public ConnectNotifyListener(Device device) {
        this.device = device;
    }
    @Override
    public void onNotify(String connectId, String topic, AMessage aMessage) {
        String jsonData = new String((byte[]) aMessage.data);
        AppLogger.i(TAG, "收到: topic=" + topic + ", content="+ jsonData);
        DeviceAction action = device.action;
        if (topic.contains(Constants.TOPIC_ORDER_SUFFIX)) {
            action.receiveOrder(convertToOrderData(jsonData));
        } else if (topic.contains(Constants.TOPIC_NOTIFY_REPLY_SUFFIX)) {
            action.receiveNotifyReply(convertToNotifyReplyData(jsonData));
        } else {
            AppLogger.w(TAG, "无法识别topic：" + topic);
        }
    }

    @Override
    public boolean shouldHandle(String connectId, String topic) {
        return true;
    }

    @Override
    public void onConnectStateChange(String connectId, ConnectState connectState) {
        AppLogger.i(TAG, "连接状态：id=" + connectId + ", state=" + connectState.name());
        if (connectState == ConnectState.CONNECTED) {
            Intent intent = new Intent(CONNECTED_ACTION);
            device.context.sendBroadcast(intent);
        }
    }

    private static OrderData convertToOrderData(String json) {
        OrderData data = new OrderData();
        try {

            JSONObject jsonObj = new JSONObject(json);

            data.amount = jsonObj.getString("amount");
            data.orderNo = jsonObj.getString("orderNo");
            data.payType = jsonObj.getString("payType");
            data.transCode = jsonObj.getString("transCode");

            // 使金额始终是两位小数
            data.amount = new BigDecimal(data.amount).setScale(2, RoundingMode.DOWN).toPlainString();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    private static NotifyReplyData convertToNotifyReplyData(String json) {
        NotifyReplyData data = new NotifyReplyData();
        try {
            JSONObject jsonObj = new JSONObject(json);
            data.channelOrderNo = jsonObj.getString("channelOrderNo");
            data.orderNo = jsonObj.getString("orderNo");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }


}
