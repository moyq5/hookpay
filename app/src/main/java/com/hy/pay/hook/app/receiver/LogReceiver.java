package com.hy.pay.hook.app.receiver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hy.pay.hook.app.R;

public class LogReceiver extends BroadcastReceiver {
    private static final int level = Log.INFO;
    private Activity activity;
    public LogReceiver(Activity activity) {
        this.activity = activity;
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        Integer level = intent.getIntExtra("level", 0);
        String msg = intent.getStringExtra("msg");
        TextView view = activity.findViewById(R.id.logConsoleView);
        if (null == msg || msg.isEmpty()) {
            view.setText("(空)");
            return;
        }

        if (level < this.level) {
            return;
        }

        String text = view.getText().toString();
        if (text.equals("(空)")) {
            view.setText("");
        } else if (text.length() > 20000) {
            int index = text.indexOf(System.getProperty("line.separator",""), text.length() - 20000);
            text = text.substring(index);
            view.setText(text);
        }
        view.append(msg);

        ScrollView scrollView = activity.findViewById(R.id.logScrollView);
        scrollView.post(() -> scrollView.fullScroll(View.FOCUS_DOWN));
    }
}
