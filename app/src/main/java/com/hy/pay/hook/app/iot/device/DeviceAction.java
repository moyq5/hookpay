package com.hy.pay.hook.app.iot.device;

import com.hy.pay.hook.app.iot.device.data.NotifyData;
import com.hy.pay.hook.app.iot.device.data.NotifyReplyData;
import com.hy.pay.hook.app.iot.device.data.OrderData;
import com.hy.pay.hook.app.iot.device.data.QrcodeData;

public interface DeviceAction {

    void receiveOrder(OrderData data);
    void sendQrcode(QrcodeData data);
    void sendNotify(NotifyData data);
    void receiveNotifyReply(NotifyReplyData data);
}
