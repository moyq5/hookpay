package com.hy.pay.hook.app.impl.unionpay;

import android.content.Intent;
import android.widget.Toast;

import com.hy.pay.hook.app.AppLogger;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrder;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrderDao;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrderDaoFactory;
import com.hy.pay.hook.app.HookContext;
import com.hy.pay.hook.app.impl.unionpay.hook.GenerateQrcodeReceiver;
import com.hy.pay.hook.app.iot.device.Device;
import com.hy.pay.hook.app.iot.device.DeviceActionAbstract;
import com.hy.pay.hook.app.iot.device.data.NotifyReplyData;
import com.hy.pay.hook.app.iot.device.data.OrderData;


public class UnionpayDeviceAction extends DeviceActionAbstract {
    private static final String TAG = UnionpayDeviceAction.class.getSimpleName();

    public UnionpayDeviceAction(Device device) {
        super(device);
    }

    @Override
    public void receiveOrder(OrderData data) {
        MoneyOrderDao dao = MoneyOrderDaoFactory.getDao();
        MoneyOrder order = dao.create(data.orderNo, Integer.parseInt(data.payType), data.amount);
        if (null == order) {
            Toast toast=Toast.makeText(device.context, "指定金额["+ data.amount +"]无法生成订单", Toast.LENGTH_SHORT);
            toast.show();
            AppLogger.w(TAG,"指定金额["+ data.amount +"]无法生成订单");
            return;
        }

        Intent intent = new Intent(GenerateQrcodeReceiver.class.getName());
        intent.putExtra("orderNo", data.orderNo);
        intent.putExtra("payType", data.payType);
        intent.putExtra("amount", data.amount);
        intent.putExtra("remark", data.remark);
        intent.putExtra("payAmount", order.getPayAmount());
        HookContext.getContext().sendBroadcast(intent);
    }

    @Override
    public void receiveNotifyReply(NotifyReplyData data) {
        MoneyOrderDao dao = MoneyOrderDaoFactory.getDao();
        dao.remove(data.channelOrderNo, data.orderNo);
    }

}
