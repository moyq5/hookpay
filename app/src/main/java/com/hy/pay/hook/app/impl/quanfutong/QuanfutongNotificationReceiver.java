package com.hy.pay.hook.app.impl.quanfutong;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.hy.pay.hook.app.AppLogger;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrder;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrderDao;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrderDaoFactory;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrderDaoImpl;
import com.hy.pay.hook.app.receiver.PayStatusReceiver;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class QuanfutongNotificationReceiver extends BroadcastReceiver {

    private static final String TAG = QuanfutongNotificationReceiver.class.getSimpleName();

    private static final String ACTION_NAME = "全付通->";

    public static final String ACTION = "cn.swiftpass.enterprise.citic";


    @Override
    public void onReceive(Context context, Intent intent) {

        final String text = intent.getStringExtra("text");

        Pattern p = Pattern.compile("收款成功，(.*?)元");
        Matcher m = p.matcher(text);
        String payAmount = null;
        while (m.find()) {
            payAmount = m.group(1);
            break;
        }

        if (null == payAmount) {
            AppLogger.w(TAG, ACTION_NAME + "无法获取金额");
            return;
        }

        AppLogger.d(TAG, ACTION_NAME + "获取金额：" + payAmount);

        MoneyOrderDao dao = MoneyOrderDaoFactory.getDao();
        MoneyOrder order = dao.updatePayStatus(payAmount, 1);
        if (null == order) {
            AppLogger.w(TAG, ACTION_NAME + "找不到金额["+ payAmount +"]对应订单");
            return;
        }

        Long expireTime = order.getAddTime() + ((MoneyOrderDaoImpl)dao).getExpireIn()*1000;
        if (expireTime < System.currentTimeMillis()) {
            String expireTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(expireTime));
            AppLogger.w(TAG, ACTION_NAME + "无效通知，金额["+ payAmount +"]对应订单["+ order.getOrderNo() +"]过期[" + expireTimeFormat + "]");
            dao.remove(order.getChannelOrderNo(), order.getOrderNo());
            return;
        }

        Intent statusIntent = new Intent();
        statusIntent.putExtra("payAmount", order.getPayAmount());
        statusIntent.putExtra("channelOrderNo", order.getOrderNo());// order.getChannelOrderNo()
        statusIntent.putExtra("payType", String.valueOf(order.getPayType()));
        statusIntent.putExtra("amount", order.getAmount());
        statusIntent.putExtra("orderNo", order.getOrderNo());
        statusIntent.putExtra("status", String.valueOf(order.getPayStatus()));
        statusIntent.setAction(PayStatusReceiver.class.getName());
        context.sendBroadcast(statusIntent);
    }
}
