package com.hy.pay.hook.app;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import de.robv.android.xposed.IXposedHookInitPackageResources;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_InitPackageResources;
import de.robv.android.xposed.callbacks.XC_LayoutInflated;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

public class TestResourcesHook implements IXposedHookLoadPackage, IXposedHookInitPackageResources {

    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam pkparam) throws Throwable {
        String packageName = pkparam.packageName;

        XposedBridge.log("加载到包1：" + packageName);

        if (!packageName.equals("cn.swiftpass.enterprise.citic"))
            return;

        XposedHelpers.findAndHookMethod(Activity.class, "onCreate", Bundle.class, new XC_MethodHook() {
                    @Override
                    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                        Object activity = param.thisObject;
                        if (!activity.getClass().toString().contains("cn.swiftpass.enterprise.ui.activity.bill.BillMainActivity")) {
                            return;
                        }

                        View view = ((Activity) param.thisObject).findViewById(new Integer(0x7f0f0623));
                        if (null == view) {
                            XposedBridge.log("未找到 filter_bottom");
                            return;
                        }
                        XposedBridge.log("找到 filter_bottom");
                        ArrayList<View> list = new ArrayList<>();
                        view.findViewsWithText(list, "确定", View.FIND_VIEWS_WITH_TEXT);
                        if (list.size() > 0) {
                            XposedBridge.log("找到确定按钮");
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    while(true) {
                                        try {
                                            Thread.sleep(30000);
                                            list.get(0).callOnClick();
                                        } catch (Exception e) {
                                            XposedBridge.log(Log.getStackTraceString(e));
                                        }
                                    }

                                }
                            }).start();
                        }
                    }
                }
        );

    }
    @Override
    public void handleInitPackageResources(XC_InitPackageResources.InitPackageResourcesParam rsParam) throws Throwable {
        String packageName = rsParam.packageName;

        XposedBridge.log("加载到包2：" + packageName);

        if (!packageName.equals("cn.swiftpass.enterprise.citic"))
            return;

        rsParam.res.hookLayout(new Integer(0x7f0f0623), new XC_LayoutInflated() {
            @Override
            public void handleLayoutInflated(LayoutInflatedParam param) throws Throwable {
                ArrayList<View> list = new ArrayList<>();
                param.view.findViewsWithText(list, "确定", View.FIND_VIEWS_WITH_TEXT);
                if (list.size() > 0) {
                    XposedBridge.log("找到确定按钮");
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            while(true) {
                                try {
                                    Thread.sleep(30000);
                                    list.get(0).callOnClick();
                                } catch (Exception e) {
                                    XposedBridge.log(Log.getStackTraceString(e));
                                }
                            }

                        }
                    }).start();
                }
            }
        });

    }
}
