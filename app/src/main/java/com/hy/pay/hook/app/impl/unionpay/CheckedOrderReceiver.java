package com.hy.pay.hook.app.impl.unionpay;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.hy.pay.hook.app.AppLogger;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrder;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrderDao;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrderDaoFactory;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrderDaoImpl;
import com.hy.pay.hook.app.receiver.PayStatusReceiver;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CheckedOrderReceiver extends BroadcastReceiver {
    private static final String TAG = CheckedOrderReceiver.class.getSimpleName();

     @Override
    public void onReceive(Context context, Intent intent) {
        final String payAmount = intent.getStringExtra("payAmount");

        MoneyOrderDao dao = MoneyOrderDaoFactory.getDao();
        MoneyOrder order = dao.updatePayStatus(payAmount, 1);
        if (null == order) {
            AppLogger.w(TAG, "找不到金额["+ payAmount +"]对应订单");
            return;
        }

        dao.remove(order.getChannelOrderNo(), order.getOrderNo());

         Long expireTime = order.getAddTime() + ((MoneyOrderDaoImpl)dao).getExpireIn()*1000;
        if (expireTime < System.currentTimeMillis()) {
            String expireTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(expireTime));
            AppLogger.w(TAG, "无效通知，金额["+ payAmount +"]对应订单["+ order.getOrderNo() +"]过期[" + expireTimeFormat + "]");
            return;
        }

        Intent statusIntent = new Intent();
        statusIntent.putExtra("payAmount", order.getPayAmount());
        statusIntent.putExtra("channelOrderNo", order.getOrderNo());// order.getChannelOrderNo()
        statusIntent.putExtra("payType", String.valueOf(order.getPayType()));
        statusIntent.putExtra("amount", order.getAmount());
        statusIntent.putExtra("orderNo", order.getOrderNo());
        statusIntent.putExtra("status", String.valueOf(order.getPayStatus()));
        statusIntent.setAction(PayStatusReceiver.class.getName());
        context.sendBroadcast(statusIntent);

    }

}
