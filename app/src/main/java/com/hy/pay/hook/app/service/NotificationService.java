package com.hy.pay.hook.app.service;

import android.app.Notification;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import com.hy.pay.hook.app.AppLogger;
import com.hy.pay.hook.app.receiver.StatusBarCmdReceiver;

public class NotificationService extends NotificationListenerService {
    private static final String TAG = NotificationService.class.getSimpleName();
    private StatusBarCmdReceiver cmdReceiver;
    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        AppLogger.d(TAG, "通知栏新消息");

        final String packageName = sbn.getPackageName();

        AppLogger.d(TAG, "通知栏->应用: " + packageName);

        Notification notification = sbn.getNotification();
        Bundle extras = notification.extras;

        String title = extras.getString(Notification.EXTRA_TITLE);
        String text = extras.getString(Notification.EXTRA_TEXT);

        AppLogger.d(TAG, "通知栏->标题："+ title +"，内容： " + text);

        Intent intent = new Intent(packageName);
        intent.putExtra("title", title);
        intent.putExtra("text", text);
        sendBroadcast(intent);

        // 删除3分钟前的通知
        StatusBarNotification[] nfcs = getActiveNotifications();
        for (StatusBarNotification nfc: nfcs) {
            if (nfc.isClearable()  && nfc.getPostTime() < System.currentTimeMillis() - 180000) {
                AppLogger.d(TAG, "通知栏->删除通知：key: "+ nfc.getKey() + "，title："+ title +"，text： " + text);
                cancelNotification(nfc.getKey());
            }
        }

    }

    @Override
    public void onListenerConnected() {
        Log.d(TAG, "onListenerConnected: ");
        super.onListenerConnected();
    }

    @Override
    public void onListenerDisconnected() {
        Log.d(TAG, "onListenerDisconnected: ");
        super.onListenerDisconnected();
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        Log.d(TAG, "onNotificationRemoved: ");
        super.onNotificationRemoved(sbn);
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate: ");
        cmdReceiver = new StatusBarCmdReceiver(this);
        IntentFilter filter = new IntentFilter();
        filter.addAction(StatusBarCmdReceiver.class.getName());
        registerReceiver(cmdReceiver, filter);

    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        unregisterReceiver(cmdReceiver);
    }
}
