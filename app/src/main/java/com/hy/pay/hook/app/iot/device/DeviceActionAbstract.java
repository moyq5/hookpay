package com.hy.pay.hook.app.iot.device;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.alink.linkkit.api.LinkKit;
import com.aliyun.alink.linksdk.cmp.connect.channel.MqttPublishRequest;
import com.aliyun.alink.linksdk.cmp.core.base.ARequest;
import com.aliyun.alink.linksdk.cmp.core.base.AResponse;
import com.aliyun.alink.linksdk.cmp.core.listener.IConnectSendListener;
import com.aliyun.alink.linksdk.tools.AError;
import com.hy.pay.hook.app.AppLogger;
import com.hy.pay.hook.app.Constants;
import com.hy.pay.hook.app.iot.device.data.NotifyData;
import com.hy.pay.hook.app.iot.device.data.QrcodeData;


public abstract class DeviceActionAbstract implements DeviceAction {
    private static final String TAG = DeviceActionAbstract.class.getSimpleName();
    protected Device device;
    public DeviceActionAbstract(Device device) {
        this.device = device;
    }

    @Override
    public void sendQrcode(QrcodeData data) {
        publish(String.format(Constants.TOPIC_QRCODE_FORMAT, device.productKey, device.deviceName), data);
    }

    @Override
    public void sendNotify(NotifyData data) {
        publish(String.format(Constants.TOPIC_NOTIFY_FORMAT, device.productKey, device.deviceName), data);
    }

    private void publish(String topic, Object data) {
        final MqttPublishRequest request = new MqttPublishRequest();
        request.isRPC = false;
        request.topic = topic;
        request.qos = 0;
        request.payloadObj = JSONObject.toJSONString(data);
        LinkKit.getInstance().publish(request, new IConnectSendListener() {
            @Override
            public void onResponse(ARequest aRequest, AResponse aResponse) {
                //AppLogger.i(TAG, "发送: " + request.payloadObj);
                AppLogger.i(TAG, "发布: topic=" + request.topic + "，content=" + request.payloadObj);
            }
            @Override
            public void onFailure(ARequest aRequest, AError aError) {
                AppLogger.w(TAG, "发送失败: topic=" + request.topic + "，content=" + request.payloadObj);
            }
        });
    }
}
