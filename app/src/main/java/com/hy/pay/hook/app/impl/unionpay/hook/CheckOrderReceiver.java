package com.hy.pay.hook.app.impl.unionpay.hook;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.hy.pay.hook.app.AppContext;
import com.hy.pay.hook.app.HookLogger;
import com.hy.pay.hook.app.impl.unionpay.CheckedOrderReceiver;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CheckOrderReceiver extends BroadcastReceiver {
    private static final String TAG = CheckOrderReceiver.class.getName();
    private static int prevTotalResult = -1;
    @Override
    public void onReceive(Context context, Intent intent) {

        Unionpayer.getInstance().reset();

        final String channelOrderNo = intent.getStringExtra("channelOrderNo");

        new Thread(() -> {
            if (null != channelOrderNo) {
                check(channelOrderNo);
            } else {
                check();
            }
        }).start();

    }

    private void check() {
        String json = HookUtils.getOrderList(1);
        if (null == json || json.isEmpty()) {
            HookLogger.w(TAG,"订单数查询结果：" + json);
            return;
        }
        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject(json);
            String resp = jsonObj.getString("resp");
            if (null == resp || !resp.equals("00")) {
                HookLogger.e(TAG,"订单数查询失败：" + jsonObj.getString("msg"));
                return;
            }
            JSONObject params = jsonObj.getJSONObject("params");
            String totalPage = params.getString("totalPage");// totalResult
            Integer totalResult = Integer.parseInt(totalPage);
            if (prevTotalResult == -1 // 首次查的不算
                    || totalResult == 0
                    || totalResult == prevTotalResult
                    || totalResult < prevTotalResult) {// 换月时可能变小
                prevTotalResult = totalResult;
                HookLogger.d(TAG,"订单数没变：" + prevTotalResult);
                return;
            }

            json = HookUtils.getOrderList(totalResult - prevTotalResult);
            if (null == json || json.isEmpty()) {
                HookLogger.w(TAG,"订单列表查询结果：" + json);
                return;
            }
            prevTotalResult = totalResult;

            jsonObj = new JSONObject(json);
            params = jsonObj.getJSONObject("params");
            JSONArray orders = params.getJSONArray("uporders");
            for (int i = 0; i < orders.length(); i++) {
                if (orders.getJSONObject(i).getString("orderStatus").equals("02")) {

                    /* 被下面代码替代
                    check(orders.getJSONObject(i).getString("orderId"));
                    */

                    Intent checkedReceiverIntent = new Intent(CheckedOrderReceiver.class.getName());
                    checkedReceiverIntent.putExtra("payAmount", orders.getJSONObject(i).getString("amount"));
                    AppContext.sendBroadcast(checkedReceiverIntent);
                }

            }


        } catch (Exception e) {
            HookLogger.e(TAG, e);
        }
    }

    private void check(String orderId) {
        String json = HookUtils.getOrderInfo(orderId);
        if (null == json || json.isEmpty()) {
            HookLogger.w(TAG, "订单["+ orderId+"]详情查询结果：" + json);
            return;
        }
        HookLogger.d(TAG, "订单["+ orderId+"]详情查询结果：" + json);
        JSONObject jsonObj;
        try {
            jsonObj = new JSONObject(json);
            String resp = jsonObj.getString("resp");
            if (null == resp || !resp.equals("00")) {
                HookLogger.e(TAG, "订单["+ orderId+"]详情查询失败：" + jsonObj.getString("msg"));
                return;
            }
            JSONObject params = jsonObj.getJSONObject("params");
            String totalAmount = params.getString("totalAmount");
            String orderStatus = params.getString("orderStatus");

            if (!orderStatus.equals("A000")) {// 当前逻辑不会发生。。
                HookLogger.d(TAG, "订单["+ orderId+"]未支付");
                return;
            }

            // 分转元
            totalAmount = new BigDecimal(totalAmount)
                    .divide(new BigDecimal("100"))
                    .setScale(2, RoundingMode.DOWN)
                    .toPlainString();

            Intent checkedReceiverIntent = new Intent(CheckedOrderReceiver.class.getName());
            checkedReceiverIntent.putExtra("payAmount", totalAmount);
            AppContext.sendBroadcast(checkedReceiverIntent);

        } catch (Exception e) {
            HookLogger.e(TAG, e);
        }

    }

}
