package com.hy.pay.hook.app.impl.sqlite.money;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.hy.pay.hook.app.impl.sqlite.code.CodeOrder;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Moyq5
 * @date 2019年3月24日
 */
public class MoneyOrderDaoImpl implements MoneyOrderDao {
	private static final String TABLE_NAME = "user_order";
	private SQLiteOpenHelper helper;
	private int range = 100;// 少分幅度
	private int expireIn =180;// 订单有效期(秒)
	public MoneyOrderDaoImpl(SQLiteOpenHelper helper) {
		this.helper = helper;
	}
	public MoneyOrderDaoImpl(SQLiteOpenHelper helper, int range) {
		this.helper = helper;
		this.range = range;
	}
	public MoneyOrderDaoImpl(SQLiteOpenHelper helper, int range, int expireIn) {
		this.helper = helper;
		this.range = range;
		this.expireIn = expireIn;
	}
	
	public MoneyOrder create(String orderNo, Integer payType, String amount) {
		if (null == amount || amount.isEmpty() || new BigDecimal(amount).compareTo(BigDecimal.ZERO) != 1) {
			return null;
		}
		SQLiteDatabase rdb = helper.getReadableDatabase();
		String payAmount = null;
		int i = 0;
		while(range >= i) {
			// 始终按两位小数
			payAmount = new BigDecimal(amount).
					subtract(new BigDecimal("" + i).divide(new BigDecimal("100"))).
					setScale(2, RoundingMode.DOWN).
					toPlainString();
			i++;
			Cursor c = rdb.rawQuery("select id from "+ TABLE_NAME +" where pay_type=? and pay_amount=? and notify_status=0 and add_time>? ", 
					new String[]{String.valueOf(payType), payAmount, "" + (System.currentTimeMillis() - expireIn * 1000)});
			if (c.moveToNext()) {
				payAmount = null;
				continue;
			}
			break;
		}
		if (null == payAmount) {
			return null;
		}

		MoneyOrder newOrder = new MoneyOrder();
		newOrder.setOrderNo(orderNo);
		newOrder.setPayType(payType);
		newOrder.setAmount(amount);
		newOrder.setPayAmount(payAmount);
		newOrder.setPayStatus(0);
		newOrder.setPayTime(0L);
		newOrder.setNotifyCount(0);
		newOrder.setNotifyStatus(0);
		newOrder.setNotifyTime(0L);
		newOrder.setAddTime(System.currentTimeMillis());
		
		ContentValues cv = new ContentValues();  
        cv.put("order_no", orderNo); 
        cv.put("pay_type", payType);
        cv.put("amount", amount);
        cv.put("pay_amount", payAmount);
        cv.put("pay_status", 0);
        cv.put("pay_time", 0);
        cv.put("notify_count", 0);
        cv.put("notify_status", 0);
        cv.put("notify_time", 0);
        cv.put("add_time", newOrder.getAddTime());
        
		SQLiteDatabase wdb = helper.getWritableDatabase();
		long id = wdb.insert(TABLE_NAME, "channel_order_no", cv);
		
		newOrder.setId(id);
		
		return newOrder;
	}
	public void updateChannelOrderNo(Long id, String channelOrderNo) {
		ContentValues cv = new ContentValues();  
        cv.put("channel_order_no", channelOrderNo);
		SQLiteDatabase wdb = helper.getWritableDatabase();
		wdb.update(TABLE_NAME, cv, "id=?", new String[]{String.valueOf(id)});  
		
	}
	public MoneyOrder updatePayStatus(String payAmount, Integer payStatus) {
		// 始终按两位小数
		payAmount = new BigDecimal(payAmount).setScale(2, RoundingMode.DOWN).toPlainString();

		SQLiteDatabase rdb = helper.getReadableDatabase();
		Cursor c = rdb.rawQuery("select * from "+ TABLE_NAME +" where pay_amount=? and pay_status=0 order by id desc limit 0, 1", 
				new String[]{payAmount});
		if (!c.moveToNext()) {
			return null;
		}
		
		long payTime = System.currentTimeMillis();
		
		ContentValues cv = new ContentValues();  
        cv.put("pay_status", payStatus);
        cv.put("pay_time", payTime);
		SQLiteDatabase wdb = helper.getWritableDatabase();
		int count = wdb.update(TABLE_NAME, cv, "id=?", new String[]{c.getString(c.getColumnIndex("id"))});
		
		MoneyOrder order = new MoneyOrder();
		order.setAddTime(c.getLong(c.getColumnIndex("add_time")));
		order.setAmount(c.getString(c.getColumnIndex("amount")));
		order.setChannelOrderNo(c.getString(c.getColumnIndex("channel_order_no")));
		order.setId(c.getLong(c.getColumnIndex("id")));
		order.setNotifyCount(c.getInt(c.getColumnIndex("notify_count")));
		order.setNotifyStatus(c.getInt(c.getColumnIndex("notify_status")));
		order.setNotifyTime(c.getLong(c.getColumnIndex("notify_time")));
		order.setOrderNo(c.getString(c.getColumnIndex("order_no")));
		order.setPayAmount(payAmount);
		order.setPayStatus(payStatus);
		order.setPayTime(payTime);
		order.setPayType(c.getInt(c.getColumnIndex("pay_type")));
		
		return order;
	}
	public void updateNotifyStatus(String channelOrderNo, String orderNo) {
		ContentValues cv = new ContentValues();  
        cv.put("notify_status", 1);
        cv.put("notify_time", System.currentTimeMillis());
		SQLiteDatabase wdb = helper.getWritableDatabase();
		int count = wdb.update(TABLE_NAME, cv, "channel_order_no=? and order_no=?", new String[]{channelOrderNo, orderNo});
		if (count == 0) {
			count = wdb.update(TABLE_NAME, cv, "channel_order_no=?", new String[]{channelOrderNo});
		}
		if (count == 0) {
			count = wdb.update(TABLE_NAME, cv, "order_no=?", new String[]{orderNo});
		}
		
	}
	public void remove(String channelOrderNo, String orderNo) {
		SQLiteDatabase wdb = helper.getWritableDatabase();
		int count = wdb.delete(TABLE_NAME, "channel_order_no=? and order_no=?", new String[]{channelOrderNo, orderNo});
		if (count == 0) {
			count = wdb.delete(TABLE_NAME, "channel_order_no=?", new String[]{channelOrderNo});
		}
		if (count == 0) {
			count = wdb.delete(TABLE_NAME, "order_no=?", new String[]{orderNo});
		}
		
	}

	public List<MoneyOrder> queryUnpaied(Long afterTime) {
		SQLiteDatabase rdb = helper.getReadableDatabase();
		Cursor c = rdb.rawQuery("select * from "+ TABLE_NAME +" where pay_status=0 and add_time>? order by id desc limit 0, 10",
				new String[]{String.valueOf(afterTime)});
		List<MoneyOrder> list = new ArrayList<MoneyOrder>();
		while (c.moveToNext()) {
			list.add(convert(c));
		}
		return list;
	}

	public List<MoneyOrder> queryNotNotified(Long beforeTime, Integer maxCount) {
		SQLiteDatabase rdb = helper.getReadableDatabase();
		Cursor c = rdb.rawQuery("select * from "+ TABLE_NAME +" where pay_status=1 and notify_status=0 and notify_time<? and notify_count<?", 
				new String[]{String.valueOf(beforeTime), String.valueOf(maxCount)});
		List<MoneyOrder> list = new ArrayList<MoneyOrder>();
		while (c.moveToNext()) {
			MoneyOrder order = new MoneyOrder();
			order.setAddTime(c.getLong(c.getColumnIndex("add_time")));
			order.setAmount(c.getString(c.getColumnIndex("amount")));
			order.setChannelOrderNo(c.getString(c.getColumnIndex("channel_order_no")));
			order.setId(c.getLong(c.getColumnIndex("id")));
			order.setNotifyCount(c.getInt(c.getColumnIndex("notify_count")));
			order.setNotifyStatus(c.getInt(c.getColumnIndex("notify_status")));
			order.setNotifyTime(c.getLong(c.getColumnIndex("notify_time")));
			order.setOrderNo(c.getString(c.getColumnIndex("order_no")));
			order.setPayAmount(c.getString(c.getColumnIndex("pay_amount")));
			order.setPayStatus(c.getInt(c.getColumnIndex("pay_status")));
			order.setPayTime(c.getLong(c.getColumnIndex("pay_time")));
			order.setPayType(c.getInt(c.getColumnIndex("pay_type")));
			list.add(order);
		}
		return list;
	}

	public MoneyOrder queryByPayAmount(String payAmount) {
		// 始终按两位小数
		payAmount = new BigDecimal(payAmount).setScale(2, RoundingMode.DOWN).toPlainString();

		SQLiteDatabase rdb = helper.getReadableDatabase();
		Cursor c = rdb.rawQuery("select * from "+ TABLE_NAME +" where pay_amount=? order by id desc limit 0, 1",
				new String[]{payAmount});
		MoneyOrder order = null;
		if (c.moveToNext()) {
			order = new MoneyOrder();
			order.setAddTime(c.getLong(c.getColumnIndex("add_time")));
			order.setAmount(c.getString(c.getColumnIndex("amount")));
			order.setChannelOrderNo(c.getString(c.getColumnIndex("channel_order_no")));
			order.setId(c.getLong(c.getColumnIndex("id")));
			order.setNotifyCount(c.getInt(c.getColumnIndex("notify_count")));
			order.setNotifyStatus(c.getInt(c.getColumnIndex("notify_status")));
			order.setNotifyTime(c.getLong(c.getColumnIndex("notify_time")));
			order.setOrderNo(c.getString(c.getColumnIndex("order_no")));
			order.setPayAmount(c.getString(c.getColumnIndex("pay_amount")));
			order.setPayStatus(c.getInt(c.getColumnIndex("pay_status")));
			order.setPayTime(c.getLong(c.getColumnIndex("pay_time")));
			order.setPayType(c.getInt(c.getColumnIndex("pay_type")));
		}
		return order;
	}

	public void updateNotifyTime(Long id) {
		SQLiteDatabase rdb = helper.getReadableDatabase();
		Cursor c = rdb.rawQuery("select notify_count from "+ TABLE_NAME +" where id=?", 
				new String[]{String.valueOf(id)});
		if (!c.moveToNext()) {
			return;
		}
		ContentValues cv = new ContentValues();  
        cv.put("notify_count", c.getInt(c.getColumnIndex("notify_count")) + 1);
        cv.put("notify_time", System.currentTimeMillis());
		SQLiteDatabase wdb = helper.getWritableDatabase();
		wdb.update(TABLE_NAME, cv, "id=?", new String[]{String.valueOf(id)});
	}
	
	public int getRange() {
		return range;
	}
	public void setRange(int range) {
		this.range = range;
	}
	public int getExpireIn() {
		return expireIn;
	}
	public void setExpireIn(int expireIn) {
		this.expireIn = expireIn;
	}


	private MoneyOrder convert(Cursor c) {
		MoneyOrder order = new MoneyOrder();
		order.setAddTime(c.getLong(c.getColumnIndex("add_time")));
		order.setAmount(c.getString(c.getColumnIndex("amount")));
		order.setChannelOrderNo(c.getString(c.getColumnIndex("channel_order_no")));
		order.setId(c.getLong(c.getColumnIndex("id")));
		order.setNotifyCount(c.getInt(c.getColumnIndex("notify_count")));
		order.setNotifyStatus(c.getInt(c.getColumnIndex("notify_status")));
		order.setNotifyTime(c.getLong(c.getColumnIndex("notify_time")));
		order.setOrderNo(c.getString(c.getColumnIndex("order_no")));
		order.setPayAmount(c.getString(c.getColumnIndex("pay_amount")));
		order.setPayStatus(c.getInt(c.getColumnIndex("pay_status")));
		order.setPayTime(c.getLong(c.getColumnIndex("pay_time")));
		order.setPayType(c.getInt(c.getColumnIndex("pay_type")));
		return order;
	}
	
}
