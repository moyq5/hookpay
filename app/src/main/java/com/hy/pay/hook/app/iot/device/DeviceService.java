package com.hy.pay.hook.app.iot.device;

public interface DeviceService {
    Device device();
    void start(Device info) throws DeviceStartedException;
    void stop();
}
