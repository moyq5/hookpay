package com.hy.pay.hook.app.iot.device.data;

public class OrderData {
    public String orderNo;
    public String payType;
    public String amount;
    public String transCode;
    public String remark;
}
