package com.hy.pay.hook.app.impl.quanfutong;

import android.app.AppOpsManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.RequiresApi;

import com.hy.pay.hook.app.AppLogger;
import com.hy.pay.hook.app.HookContext;
import com.hy.pay.hook.app.MainAbstract;
import com.hy.pay.hook.app.impl.quanfutong.hook.CheckOrderReceiver;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrder;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrderDao;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrderDaoFactory;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrderDaoImpl;
import com.hy.pay.hook.app.impl.sqlite.money.MoneyOrderDbHelper;
import com.hy.pay.hook.app.iot.device.Device;
import com.hy.pay.hook.app.service.NotificationService;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;


public class QuanfutongMain extends MainAbstract {
    private static final String TAG = QuanfutongMain.class.getSimpleName();

    private QuanfutongNotificationReceiver notificationReceiver;

    private CheckedOrderReceiver checkedReceiver;

    private Thread checkOrderService;

    public QuanfutongMain(Device device) {
        super(device);
        this.notificationReceiver = new QuanfutongNotificationReceiver();
        this.checkedReceiver = new CheckedOrderReceiver();
    }

    @Override
    protected void createInit() {
        device.action = new QuanfutongDeviceAction(device.context, device);

        device.context.registerReceiver(notificationReceiver,
                new IntentFilter(QuanfutongNotificationReceiver.ACTION));
        device.context.registerReceiver(checkedReceiver,
                new IntentFilter(CheckedOrderReceiver.class.getName()));

        MoneyOrderDbHelper helper = new MoneyOrderDbHelper(device.context, device.deviceName + ".db", null, 1);
        MoneyOrderDao dao = new MoneyOrderDaoImpl(helper, 300, 3000);// TODO
        MoneyOrderDaoFactory.setDao(dao);

        if (true) {// !isNotificationEnabled(device.context)
            Intent settingIntent = new Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS);
            device.context.startActivity(settingIntent);
        }

        Intent listenerIntent = new Intent(device.context, NotificationService.class);
        device.context.startService(listenerIntent);

        checkOrderService = startCheckService();

    }

    @Override
    public void destroy() {
        checkOrderService.interrupt();
        try {
            device.context.unregisterReceiver(notificationReceiver);
            device.context.unregisterReceiver(checkedReceiver);
        } catch (IllegalArgumentException e) {
            if (!e.getMessage().contains("Receiver not registered")) {
                throw e;
            }
        }
        super.destroy();
    }


    private Thread startCheckService() {
        Thread thread = new Thread(() -> {
            Intent intent = new Intent(CheckOrderReceiver.class.getName());
            try {
                while(true) {
                    MoneyOrderDao dao = MoneyOrderDaoFactory.getDao();
                    List<MoneyOrder> list = dao.queryUnpaied(System.currentTimeMillis() - 300000);// TODO 5分钟内的订单
                    AppLogger.i(TAG,"最近有" + list.size() + "条未支付订单");

                    if(list.size() > 0) {
                        HookContext.sendBroadcast(intent);
                    }

                    Thread.sleep(30000);
                }
            } catch (InterruptedException e) {
                AppLogger.v(TAG, "", e);
            } catch (Exception e) {
                AppLogger.e(TAG, "", e);
            }

        });
        thread.start();
        return thread;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static boolean isNotificationEnabled(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //8.0手机以上
            if (((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).getImportance() == NotificationManager.IMPORTANCE_NONE) {
                return false;
            }
        }
        String CHECK_OP_NO_THROW = "checkOpNoThrow";
        String OP_POST_NOTIFICATION = "OP_POST_NOTIFICATION";
        AppOpsManager mAppOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        ApplicationInfo appInfo = context.getApplicationInfo();
        String pkg = context.getApplicationContext().getPackageName();
        int uid = appInfo.uid;
        Class appOpsClass = null;
        try {
            appOpsClass = Class.forName(AppOpsManager.class.getName());
            Method checkOpNoThrowMethod = appOpsClass.getMethod(CHECK_OP_NO_THROW, Integer.TYPE, Integer.TYPE, String.class);
            Field opPostNotificationValue = appOpsClass.getDeclaredField(OP_POST_NOTIFICATION);
            int value = (Integer) opPostNotificationValue.get(Integer.class);
            return ((Integer) checkOpNoThrowMethod.invoke(mAppOps, value, uid, pkg) == AppOpsManager.MODE_ALLOWED);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }



}
