package com.hy.pay.hook.app;

import de.robv.android.xposed.callbacks.XC_LoadPackage;

public interface HookPackage {

    public void init(XC_LoadPackage.LoadPackageParam param);
}
