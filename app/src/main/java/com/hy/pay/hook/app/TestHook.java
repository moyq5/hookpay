package com.hy.pay.hook.app;

import com.hy.pay.hook.app.impl.quanfutong.hook.QuanfutongHook;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

public class TestHook implements IXposedHookLoadPackage {

    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam pkparam) throws Throwable {
        String packageName = pkparam.packageName;

        XposedBridge.log("加载到包：" + packageName);

        QuanfutongHook.getInstance().init(pkparam);
    }

}
