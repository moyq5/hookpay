package com.hy.pay.hook.app.impl.quanfutong.hook;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.hy.pay.hook.app.HookContext;
import com.hy.pay.hook.app.HookLogger;
import com.hy.pay.hook.app.HookPackage;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

public class QuanfutongHook implements HookPackage {
    private static final String TAG = QuanfutongHook.class.getSimpleName();

    private static final QuanfutongHook hook = new QuanfutongHook();

    private MyHandler handler = new MyHandler();

    private Object orderActivity;

    private Boolean inited = false;

    private QuanfutongHook() {}

    public static QuanfutongHook getInstance() {
        return hook;
    }

    @Override
    public void init(XC_LoadPackage.LoadPackageParam param) {
        if (inited) {
            return;
        }

        String packageName = param.packageName;

        if (!packageName.equals("cn.swiftpass.enterprise.citic"))
            return;

        hookOrderActivity();

        inited = true;

    }

    public void refreshOrderList() {
        if (null == orderActivity) {
            HookLogger.w(TAG, "请打开全付通app订单列表页");
            return;
        }
        try {
            handler.setContext((Context)orderActivity);
            handler.sendMessage(Message.obtain());
            Method method = orderActivity.getClass().getDeclaredMethod("onRefresh");
            method.setAccessible(true);
            method.invoke(orderActivity);
        } catch (Exception e) {
            HookLogger.e(TAG, e);
        }
    }


    public List<Object> orderList() {
        try {
            Field field = orderActivity.getClass().getDeclaredField("orderList");
            field.setAccessible(true);
            return (List<Object>)field.get(orderActivity);
        } catch (Exception e) {
            HookLogger.e(TAG, e);
        }
        return new ArrayList<>();
    }


    private void hookOrderActivity() {
        XposedHelpers.findAndHookMethod(Activity.class, "onCreate", Bundle.class, new XC_MethodHook() {
                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    if (null != orderActivity || !param.thisObject.getClass().toString().contains("cn.swiftpass.enterprise.ui.activity.bill.BillMainActivity")) {
                        return;
                    }
                    orderActivity = param.thisObject;
                    hookContext((Context) orderActivity);
                    registerReceiver((Context) orderActivity);
                }
            }
        );
    }

    private void hookContext(Context context) {
        XposedHelpers.findAndHookMethod(HookContext.class, "getContext", new XC_MethodHook() {
            protected void afterHookedMethod(XC_MethodHook.MethodHookParam mhparam)
                    throws Throwable {
                mhparam.setResult(context);
            }
        });
    }


    private void registerReceiver(Context context) {
        IntentFilter checkOrderFilter = new IntentFilter();
        checkOrderFilter.addAction(CheckOrderReceiver.class.getName());
        context.registerReceiver(new CheckOrderReceiver(), checkOrderFilter);
    }


    @SuppressLint("HandlerLeak")
    private static class MyHandler extends Handler {

        private Context context;

        @Override
        public void handleMessage(Message msg) {
            Toast toast = Toast.makeText(context, "刷新", Toast.LENGTH_SHORT);
            toast.getView().setPadding(5,5,5,5);
            toast.show();
        }
        public Context getContext() {
            return context;
        }

        public void setContext(Context context) {
            this.context = context;
        }

    }



    private View findSearchBtn(View view) {
        ArrayList<View> list = new ArrayList<>();
        view.findViewsWithText(list, "确定", View.FIND_VIEWS_WITH_TEXT);
        if (list.size() > 0) {
            return list.get(0);
        } else if(true) {
            return null;
        }
        if (view instanceof TextView) {
            XposedBridge.log("view: " + view.getClass());
            XposedBridge.log("view id: " + view.getId());
            XposedBridge.log("view text: " + ((TextView) view).getText());
        }
        if (view instanceof Button && ((Button) view).getText().equals("确定")) {
            return view;
        }
        if (view instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) view;
            for (int i = 0; i < group.getChildCount(); i++) {
                View child = findSearchBtn(group.getChildAt(i));
                if (null != child) {
                    return child;
                }
            }
        }
        return null;
    }


    private void hookBillMainActivity(ClassLoader loader) {
        XposedHelpers.findAndHookMethod("cn.swiftpass.enterprise.ui.activity.bill.BillMainActivity", loader, "onCreate", Bundle.class, new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                Object activity = param.thisObject;
                XposedBridge.log("class:" + activity.getClass().toString());
                XposedBridge.log("fields####################################");
                Field[] allField = activity.getClass().getDeclaredFields();
                XposedBridge.log("fields size:" + allField.length);
                for (Field field : allField) {
                    field.setAccessible(true);
                    XposedBridge.log("fieldClass:" + field.getType());
                    XposedBridge.log("fieldName:" + field.getName());
                    XposedBridge.log("fieldValue:" + field.get(activity));
                }
                XposedBridge.log("methods####################################");
                Method[] allMethod = activity.getClass().getDeclaredMethods();
                XposedBridge.log("methods size:" + allMethod.length);
                for (Method method : allMethod) {
                    method.setAccessible(true);
                    XposedBridge.log("methodClass:" + method);
                    XposedBridge.log("methodName:" + method.getName());
                    XposedBridge.log("methodParameterTypes:" + method.getParameterTypes());
                    XposedBridge.log("methodResultType:" + method.getReturnType());
                }
                final Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Thread.sleep(30000);
                            final Method onRefresh = param.thisObject.getClass().getMethod("onRefresh");
                            onRefresh.invoke(param.thisObject);
                        } catch (Exception e) {
                            XposedBridge.log(Log.getStackTraceString(e));
                        }

                    }
                });
            }
        });
    }

    private void hookOrderListRefresh(ClassLoader loader) {
        XposedHelpers.findAndHookMethod("cn.swiftpass.enterprise.ui.activity.bill.BillMainActivity", loader, "onRefresh", new XC_MethodHook() {
            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                XposedBridge.log("订单列表刷新");
                Toast toast=Toast.makeText((Context) param.thisObject, "列表刷新", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }



}
