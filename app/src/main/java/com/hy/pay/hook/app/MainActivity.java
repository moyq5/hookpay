package com.hy.pay.hook.app;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;

import com.hy.pay.hook.app.fragment.FragmentAdapter;
import com.hy.pay.hook.app.fragment.LogFragment;
import com.hy.pay.hook.app.fragment.SettingFragment;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.BezierPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<Fragment> frags;
    private List<String> titles;
    private ViewPager pager;
    private MagicIndicator indicator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        initData();
        initPager();
        initIndicator();
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                indicator.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

            @Override
            public void onPageSelected(int position) {
                indicator.onPageSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                indicator.onPageScrollStateChanged(state);
            }
        });
        ViewPagerHelper.bind(indicator, pager);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(false);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void initIndicator() {
        indicator= findViewById(R.id.top_indicator);
        CommonNavigator navigator = new CommonNavigator(this);
        navigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return frags.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int i) {
                SimplePagerTitleView titleView=new SimplePagerTitleView(context);
                titleView.setText(titles.get(i));
                titleView.setTextSize(18);
                titleView.setNormalColor(Color.LTGRAY);
                titleView.setSelectedColor(Color.WHITE);
                titleView.setOnClickListener(v -> pager.setCurrentItem(i));
                return titleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                BezierPagerIndicator indicator = new BezierPagerIndicator(context);
                indicator.setColors(Color.MAGENTA, Color.YELLOW, Color.CYAN, Color.GREEN);
                return indicator;
            }
        });
        indicator.setNavigator(navigator);
    }

    private void initPager() {
        pager = findViewById(R.id.frag_pager);
        FragmentAdapter adapter=new FragmentAdapter(getSupportFragmentManager(), frags);
        pager.setAdapter(adapter);

    }

    private void initData() {
        SettingFragment settingFragment = new SettingFragment();
        LogFragment logFragment = new LogFragment();

        Intent intent = getIntent();
        String settingData = intent.getStringExtra("settingData");
        if (settingData != null) {
            Bundle bundle = new Bundle();
            bundle.putString("settingData",settingData);
            settingFragment.setArguments(bundle);
        }

        frags=new ArrayList<>();
        frags.add(settingFragment);
        frags.add(logFragment);

        titles=new ArrayList<>();
        titles.add("设置");
        titles.add("日志");
    }
}
