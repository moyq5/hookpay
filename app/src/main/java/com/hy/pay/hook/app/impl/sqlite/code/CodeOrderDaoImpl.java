package com.hy.pay.hook.app.impl.sqlite.code;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Moyq5
 * @date 2019年3月24日
 */
public class CodeOrderDaoImpl implements CodeOrderDao {
	private static final String TABLE_NAME = "user_order";
	private SQLiteOpenHelper helper;
	public CodeOrderDaoImpl(SQLiteOpenHelper helper) {
		this.helper = helper;
	}

	public CodeOrder addOrder(CodeOrder order) {
		SQLiteDatabase rdb = helper.getReadableDatabase();
		Cursor c = rdb.rawQuery("select * from "+ TABLE_NAME +" where order_no=? and channel_order_no=? limit 0, 1",
				new String[]{order.getOrderNo(), order.getChannelOrderNo()});
		if (c.moveToNext()) {
			return null;
		}

		order.setPayStatus(0);
		order.setPayTime(0L);
		order.setNotifyCount(0);
		order.setNotifyStatus(0);
		order.setNotifyTime(0L);
		order.setAddTime(System.currentTimeMillis());

		ContentValues cv = new ContentValues();
		cv.put("order_no", order.getOrderNo());
		cv.put("pay_type", order.getPayType());
		cv.put("amount", order.getAmount());
		cv.put("channel_order_no", order.getChannelOrderNo());
		cv.put("pay_amount", order.getPayAmount());
		cv.put("pay_status", order.getPayStatus());
		cv.put("pay_time", order.getPayTime());
		cv.put("notify_count", order.getNotifyCount());
		cv.put("notify_status", order.getNotifyStatus());
		cv.put("notify_time", order.getNotifyTime());
		cv.put("add_time", order.getAddTime());

		SQLiteDatabase wdb = helper.getWritableDatabase();
		long id = wdb.insert(TABLE_NAME, "", cv);

		order.setId(id);

		return order;
	}

	public void updatePayStatus(String orderNo, Integer payStatus) {

		long payTime = System.currentTimeMillis();
		
		ContentValues cv = new ContentValues();  
        cv.put("pay_status", payStatus);
        cv.put("pay_time", System.currentTimeMillis());
		SQLiteDatabase wdb = helper.getWritableDatabase();
		wdb.update(TABLE_NAME, cv, "order_no=?", new String[]{orderNo});

	}
	public int updateNotifyStatus(String orderNo) {
		ContentValues cv = new ContentValues();  
        cv.put("notify_status", 1);
        cv.put("notify_time", System.currentTimeMillis());
		SQLiteDatabase wdb = helper.getWritableDatabase();
		return wdb.update(TABLE_NAME, cv, "order_no=?", new String[]{orderNo});
	}

	public int remove(String channelOrderNo, String orderNo) {
		SQLiteDatabase wdb = helper.getWritableDatabase();
		return wdb.delete(TABLE_NAME, "channel_order_no=? and order_no=?", new String[]{channelOrderNo, orderNo});

	}

	public List<CodeOrder> queryUnpaied(Long afterTime) {
		SQLiteDatabase rdb = helper.getReadableDatabase();
		Cursor c = rdb.rawQuery("select * from "+ TABLE_NAME +" where pay_status=0 and add_time>? order by id desc limit 0, 10",
				new String[]{String.valueOf(afterTime)});
		List<CodeOrder> list = new ArrayList<CodeOrder>();
		while (c.moveToNext()) {
			list.add(convert(c));
		}
		return list;
	}

	public List<CodeOrder> queryUnnotified(Long beforeTime, Integer maxCount) {
		SQLiteDatabase rdb = helper.getReadableDatabase();
		Cursor c = rdb.rawQuery("select * from "+ TABLE_NAME +" where pay_status=1 and notify_status=0 and notify_time<? and notify_count<?",
				new String[]{String.valueOf(beforeTime), String.valueOf(maxCount)});
		List<CodeOrder> list = new ArrayList<CodeOrder>();
		while (c.moveToNext()) {
			list.add(convert(c));
		}
		return list;
	}

	public CodeOrder getOneByChannelOrderNo(String channelOrderNo) {
		SQLiteDatabase rdb = helper.getReadableDatabase();
		Cursor c = rdb.rawQuery("select * from "+ TABLE_NAME +" where channel_order_no=? limit 0, 1",
				new String[]{channelOrderNo});
		if (c.moveToNext()) {
			return convert(c);
		}
		return null;
	}

	public void updateNotifyTime(String channelOrderNo, String orderNo) {
		SQLiteDatabase rdb = helper.getReadableDatabase();
		Cursor c = rdb.rawQuery("select notify_count from "+ TABLE_NAME +" where channel_order_no=? and order_no=?",
				new String[]{channelOrderNo, orderNo});
		if (!c.moveToNext()) {
			return;
		}
		ContentValues cv = new ContentValues();  
        cv.put("notify_count", c.getInt(c.getColumnIndex("notify_count")) + 1);
        cv.put("notify_time", System.currentTimeMillis());
		SQLiteDatabase wdb = helper.getWritableDatabase();
		wdb.update(TABLE_NAME, cv, "channel_order_no=? and order_no=?", new String[]{channelOrderNo, orderNo});
	}

	private CodeOrder convert(Cursor c) {
		CodeOrder order = new CodeOrder();
		order.setAddTime(c.getLong(c.getColumnIndex("add_time")));
		order.setAmount(c.getString(c.getColumnIndex("amount")));
		order.setChannelOrderNo(c.getString(c.getColumnIndex("channel_order_no")));
		order.setId(c.getLong(c.getColumnIndex("id")));
		order.setNotifyCount(c.getInt(c.getColumnIndex("notify_count")));
		order.setNotifyStatus(c.getInt(c.getColumnIndex("notify_status")));
		order.setNotifyTime(c.getLong(c.getColumnIndex("notify_time")));
		order.setOrderNo(c.getString(c.getColumnIndex("order_no")));
		order.setPayAmount(c.getString(c.getColumnIndex("pay_amount")));
		order.setPayStatus(c.getInt(c.getColumnIndex("pay_status")));
		order.setPayTime(c.getLong(c.getColumnIndex("pay_time")));
		order.setPayType(c.getInt(c.getColumnIndex("pay_type")));
		return order;
	}
}
