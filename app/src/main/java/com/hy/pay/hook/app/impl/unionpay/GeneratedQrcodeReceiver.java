package com.hy.pay.hook.app.impl.unionpay;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.hy.pay.hook.app.iot.device.Device;
import com.hy.pay.hook.app.iot.device.data.QrcodeData;

public class GeneratedQrcodeReceiver extends BroadcastReceiver {

    private Device device;

    public GeneratedQrcodeReceiver(Device device) {
        this.device = device;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final QrcodeData qrcodeData = new QrcodeData();
        qrcodeData.orderNo = intent.getStringExtra("orderNo");
        qrcodeData.payType = intent.getStringExtra("payType");
        qrcodeData.amount = intent.getStringExtra("amount");
        qrcodeData.channelOrderNo = intent.getStringExtra("channelOrderNo");
        qrcodeData.payAmount = intent.getStringExtra("payAmount");
        qrcodeData.qrcode = intent.getStringExtra("qrcode");
        device.action.sendQrcode(qrcodeData);
    }

}
