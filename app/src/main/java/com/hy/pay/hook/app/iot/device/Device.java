package com.hy.pay.hook.app.iot.device;

import android.content.Context;

import com.aliyun.alink.linksdk.cmp.core.listener.IConnectNotifyListener;

public class Device {

    public String productKey;
    public String deviceName;
    public String deviceSecret;
    public String qrcodeId;
    public Context context;
    public IConnectNotifyListener notifyListener;
    public DeviceAction action;

}
