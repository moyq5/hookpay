package com.hy.pay.hook.app;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.hy.pay.hook.app.receiver.LogReceiver;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class AppLogger {

    public static Context context;

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        AppLogger.context = context;
    }

    public static int v(String tag, String msg) {
        sendLog(Log.VERBOSE, msg);
        return Log.v(tag, msg);
    }

    public static int v(String tag, String msg, Throwable tr) {
        sendLog(Log.VERBOSE, msg);
        sendLog(Log.VERBOSE, Log.getStackTraceString(tr));
        return Log.v(tag, msg, tr);
    }

    public static int d(String tag, String msg) {
        sendLog(Log.DEBUG, msg);
        return Log.d(tag, msg);
    }

    public static int d(String tag, String msg, Throwable tr) {
        sendLog(Log.DEBUG, msg);
        sendLog(Log.DEBUG, Log.getStackTraceString(tr));
        return Log.d(tag, msg, tr);
    }

    public static int i(String tag, String msg) {
        sendLog(Log.INFO, msg);
        return Log.i(tag, msg);
    }

    public static int i(String tag, String msg, Throwable tr) {
        sendLog(Log.INFO, msg);
        sendLog(Log.INFO, Log.getStackTraceString(tr));
        return Log.i(tag, msg, tr);
    }

    public static int w(String tag, String msg) {
        sendLog(Log.WARN, msg);
        return Log.w(tag, msg);
    }

    public static int w(String tag, String msg, Throwable tr) {
        sendLog(Log.WARN, msg);
        sendLog(Log.WARN, Log.getStackTraceString(tr));
        return Log.w(tag, msg, tr);
    }

    public static int w(String tag, Throwable tr) {
        sendLog(Log.WARN, Log.getStackTraceString(tr));
        return Log.w(tag, "", tr);
    }

    public static int e(String tag, String msg) {
        sendLog(Log.ERROR, msg);
        return Log.e(tag, msg);
    }

    public static int e(String tag, String msg, Throwable tr) {
        sendLog(Log.ERROR, msg);
        sendLog(Log.ERROR, Log.getStackTraceString(tr));
        return Log.e(tag, msg, tr);
    }

    public static void sendLog(int level, String msg) {
        if (null == context) return;
        Intent intent = new Intent();
        intent.putExtra("level", level);
        intent.putExtra("msg",   new SimpleDateFormat("MM-dd HH:mm:ss").format(new Date()) + " " + msg + System.getProperty("line.separator"));
        intent.setAction(LogReceiver.class.getName());
        context.sendBroadcast(intent);
    }

    public static void clearLog() {
        if (null == context) return;
        Intent intent = new Intent();
        intent.setAction(LogReceiver.class.getName());
        context.sendBroadcast(intent);
    }

}
