package com.hy.pay.hook.app.impl.sqlite.money;

public abstract class MoneyOrderDaoFactory {
    private static MoneyOrderDao dao;
    public static void setDao(MoneyOrderDao dao) {
        MoneyOrderDaoFactory.dao = dao;
    }
    public static MoneyOrderDao getDao() {
        return MoneyOrderDaoFactory.dao;
    }
}
