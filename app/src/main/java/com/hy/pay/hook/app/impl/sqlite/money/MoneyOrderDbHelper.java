package com.hy.pay.hook.app.impl.sqlite.money;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class MoneyOrderDbHelper extends SQLiteOpenHelper {

	public MoneyOrderDbHelper(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		final String createTable = "create table user_order(" 
				+ "id INTEGER primary key autoincrement,"
				+ "order_no varchar(30)," 
				+ "channel_order_no varchar(30)," 
				+ "pay_type int(1),"
				+ "amount decimal(10,2)," 
				+ "pay_amount decimal(10,2),"
				+ "add_time bigint(15),"
				+ "pay_status int(1),"
				+ "pay_time bigint(15),"
				+ "notify_count int(8),"
				+ "notify_status int(1),"
				+ "notify_time bigint(15))";
		db.execSQL(createTable);
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {

	}
}