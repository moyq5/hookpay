package com.hy.pay.hook.app.impl.sqlite.money;

import com.hy.pay.hook.app.impl.sqlite.code.CodeOrder;

import java.util.List;

/**
 * @author Moyq5
 * @date 2019年3月24日
 */
public interface MoneyOrderDao {

	/**
	 * 创建订单
	 * @author Moyq5
	 * @date 2019年3月24日
	 * @param orderNo
	 * @param payType
	 * @param amount
	 * @return
	 */
	MoneyOrder create(String orderNo, Integer payType, String amount);

	/**
	 * 更新本地单号
	 * @author Moyq5
	 * @date 2019年3月24日
	 * @param id
	 * @param channelOrderNo
	 */
	void updateChannelOrderNo(Long id, String channelOrderNo);

	/**
	 * 根据实际收到款金额更新订单支付状态
	 * @author Moyq5
	 * @date 2019年3月24日
	 * @param payAmount
	 * @param payStatus
	 * @return
	 */
	MoneyOrder updatePayStatus(String payAmount, Integer payStatus);

	/**
	 * 根据单号更新通知状态
	 * @author Moyq5
	 * @date 2019年3月24日
	 * @param channelOrderNo
	 * @param orderNo
	 */
	void updateNotifyStatus(String channelOrderNo, String orderNo);

	/**
	 * 根据单号删除订单
	 * @author Moyq5
	 * @date 2019年3月24日
	 * @param channelOrderNo
	 * @param orderNo
	 */
	void remove(String channelOrderNo, String orderNo);

	/**
	 * 查询最近未付款订单
	 * @param afterTime 查此时间后的订单
	 * @return
	 */
	List<MoneyOrder> queryUnpaied(Long afterTime);


	/**
	 * 获取未通知成功的订单
	 * @author Moyq5
	 * @date 2019年3月24日
	 * @param beforeTime 上将通知时间小于这个时间
	 * @param maxCount 通知次数少于该值
	 * @return
	 */
	List<MoneyOrder> queryNotNotified(Long beforeTime, Integer maxCount);

	/**
	 * 根据付款金额查订单
	 * @param payAmount
	 * @return
	 */
	MoneyOrder queryByPayAmount(String payAmount);

	/**
	 * 更新通知时间和通知次数
	 * @author Moyq5
	 * @date 2019年3月24日
	 * @param id
	 */
	void updateNotifyTime(Long id);

}
