package com.hy.pay.hook.app.iot.device;

public class DeviceStartedException extends DeviceException {

    public DeviceStartedException() {
        super();
    }

    public DeviceStartedException(String message) {
        super(message);
    }

    public DeviceStartedException(String message, Throwable cause) {
        super(message, cause);
    }

    public DeviceStartedException(Throwable cause) {
        super(cause);
    }

    protected DeviceStartedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
