package com.hy.pay.hook.app.impl.unionpay.hook;

import android.app.Activity;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;

import com.hy.pay.hook.app.HookContext;
import com.hy.pay.hook.app.HookLogger;

import org.json.JSONObject;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

public class UnionpayHook implements IXposedHookLoadPackage {
    private static final String TAG = UnionpayHook.class.getSimpleName();
    private Boolean inited = false;
    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam pkparam) throws Throwable {
        String packageName = pkparam.packageName;
        XposedBridge.log("加载到包：" + packageName);
        if (!"com.unionpay".equals(packageName)) {
            return;
        }

        HookUtils.setClassLoader(pkparam.classLoader);

        if (inited) {
            return;
        }
        inited = true;

        Object[] objArr = new Object[2];

        objArr[0] = String.class;
        objArr[1] = new XC_MethodHook() {
            protected void afterHookedMethod(XC_MethodHook.MethodHookParam mhParam)
                    throws Throwable {
                super.afterHookedMethod(mhParam);
                if (mhParam.args[0].toString().equals("com.unionpay.push.UPPushService")) {
                    hookPushService((Class) mhParam.getResult());
                }
            }
        };
        XposedHelpers.findAndHookMethod(ClassLoader.class, "loadClass", objArr);


        objArr = new Object[2];
        objArr[0] = Bundle.class;
        objArr[1] = new XC_MethodHook() {
            protected void afterHookedMethod(XC_MethodHook.MethodHookParam mhparam)
                    throws Throwable {
                Activity activity = (Activity) mhparam.thisObject;
                if (activity.getClass().toString().contains("com.unionpay.activity.UPActivityMain")) {
                    registerReceiver(activity);
                    hookContext(activity);
                }
            }
        };
        XposedHelpers.findAndHookMethod(Activity.class, "onCreate", objArr);
    }

    private void registerReceiver(Activity activity) {

        XposedBridge.log("first init ActivityMain OK -> " + activity.getClass().toString());

        Unionpayer payer = Unionpayer.getInstance();
        payer.setApp(activity.getApplication());

        IntentFilter genQrcodeFilter = new IntentFilter();
        genQrcodeFilter.addAction(GenerateQrcodeReceiver.class.getName());
        activity.registerReceiver(new GenerateQrcodeReceiver(), genQrcodeFilter);

        IntentFilter checkOrderFilter = new IntentFilter();
        checkOrderFilter.addAction(CheckOrderReceiver.class.getName());
        activity.registerReceiver(new CheckOrderReceiver(), checkOrderFilter);

    }

    private void hookPushService(Class clazz) {
        XposedBridge.log("first init PushService OK -> " + clazz.toString());

        Object[] objArr = new Object[2];
        objArr[0] = String.class;
        objArr[1] = new XC_MethodHook() {
            protected void beforeHookedMethod(XC_MethodHook.MethodHookParam mhParam)
                    throws Throwable {
                String msg = (String) mhParam.args[0];
                HookLogger.i(TAG, "收到通知：" + msg);
                if ((msg.contains("付款")) && (msg.contains("动账通知"))) {
                    String alert = new JSONObject(msg).getJSONObject("body").getString("alert");

                    String[] strArr = alert.split("元,")[0].split("通过扫码向您付款");
                    if (strArr.length == 2) {
                        /*
                        Intent checkedReceiverIntent = new Intent(CheckedOrderReceiver.class.getName());
                        checkedReceiverIntent.putExtra("payAmount", strArr[1]);
                        AppContext.sendBroadcast(checkedReceiverIntent);
                        */
                        /* 被上面代码替代
                        Intent intent = new Intent(CheckOrderReceiver.class.getName());
                        intent.putExtra("user", strArr[0]);
                        intent.putExtra("money", strArr[1]);
                        HookContext.sendBroadcast(intent);
                        */
                    }
                }
            }
        };
        XposedHelpers.findAndHookMethod(clazz, "c", objArr);
    }

    private void hookContext(Context context) {
        Object[] objArr = new Object[1];
        objArr[0] = new XC_MethodHook() {
            protected void afterHookedMethod(XC_MethodHook.MethodHookParam mhparam)
                    throws Throwable {
                XposedBridge.log("init unionpay app Context：" + context);
                mhparam.setResult(context);
            }
        };
        XposedHelpers.findAndHookMethod(HookContext.class, "getContext", objArr);
    }

}
