package com.hy.pay.hook.app.impl.sqlite.money;

import java.util.List;

/**
 * Hello world!
 *
 */
public class Demo {
	public static void main(String[] args) {
		System.out.println("Hello World!");
		MoneyOrderDbHelper helper = new MoneyOrderDbHelper(null, "CodeOrder.db", null, 1);
		MoneyOrderDao dao = new MoneyOrderDaoImpl(helper);
		
		// 下单参数
		String orderNo = null;
		Integer payType = null;
		String amount = null;
		
		// 创建订单记录
		MoneyOrder order = dao.create(orderNo, payType, amount);
		if (null == order) {
			// 无法可用订单。。。
			return;
		}
		
		// 根据可用金额生成本地订单，获取本地单号和二维码
		String payAmount = order.getPayAmount();
		String channelOrderNo = "本地单号";
		String payQrcode = "收款码";
		
		// 记录本地单号
		dao.updateChannelOrderNo(order.getId(), channelOrderNo);
		
		// 上传收款码。。。
		
		// 根据收到金额更新订单状态,并获取对应订单
		String successAmount = "";
		
		MoneyOrder successOrder = dao.updatePayStatus(successAmount, 1);// 1是成功状态
		if (null == successOrder) {
			// 无匹配订单。。。
			return;
		}
		
		// 根据successOrder信息发送收款通知。。。
		// 并更新通知时间
		dao.updateNotifyTime(successOrder.getId());
		
		// 收到收款通知响应
		String replyOrderNo = "";
		String replyChannelOrderNo = "";
		
		// 更新订单通知状态，或者直接删除，二选一
		dao.updateNotifyStatus(replyChannelOrderNo, replyOrderNo);
		dao.remove(replyChannelOrderNo, replyOrderNo);
		
		
		// 通知重发机制。。
		
		// 获取未通知成功的订单
		List<MoneyOrder> list = dao.queryNotNotified(System.currentTimeMillis() - 10000, 5);// 10秒钟前发过通知,且通知次数少于5次的订单
		for (MoneyOrder item: list) {
			// 根据item信息发送收款通知。。。
			// 并更新通知时间
			dao.updateNotifyTime(item.getId());
		}
	}
}
