package com.hy.pay.hook.app.fragment;

import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hy.pay.hook.app.AppLogger;
import com.hy.pay.hook.app.R;
import com.hy.pay.hook.app.receiver.LogReceiver;

public class LogFragment extends Fragment {

    private LogReceiver logReceiver;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLogger.setContext(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_log, container, false);

        logReceiver = new LogReceiver(getActivity());
        getContext().registerReceiver(logReceiver, new IntentFilter(LogReceiver.class.getName()));

        return view;

    }

    @Override
    public void onDestroyView() {
        try {
            if (null != logReceiver) {
                getContext().unregisterReceiver(logReceiver);
            }
        } catch (IllegalArgumentException e) {
            if (e.getMessage().contains("Receiver not registered")) {
            } else {
                throw e;
            }
        }

        super.onDestroyView();
    }
}
