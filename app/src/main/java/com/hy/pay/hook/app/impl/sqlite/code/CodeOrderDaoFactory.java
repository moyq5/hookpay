package com.hy.pay.hook.app.impl.sqlite.code;

public abstract class CodeOrderDaoFactory {
    private static CodeOrderDao dao;
    public static void setDao(CodeOrderDao dao) {
        CodeOrderDaoFactory.dao = dao;
    }
    public static CodeOrderDao getDao() {
        return CodeOrderDaoFactory.dao;
    }
}
