package com.hy.pay.hook.app.impl.unionpay.hook;

import android.app.Activity;
import android.app.Application;

public class Unionpayer {
    private static final Unionpayer payer = new Unionpayer();
    private Application app;
    private Activity activity;
    private String cityCd;
    private String virtualCardNo;
    private String xTid;
    private String sid;
    private String urid;
    private String sessionId;
    private String gray;

    private Unionpayer() {}

    public static Unionpayer getInstance() {
        return payer;
    }

    public void reset() {
        cityCd = null;
        virtualCardNo = null;
        xTid = null;
        sid = null;
        urid = null;
        sessionId = null;
        gray = null;
    }

    public Application getApp() {
        return app;
    }

    public void setApp(Application app) {
        this.app = app;
    }

    public String getCityCd() {
        if (null == cityCd) {
            cityCd = HookUtils.getHookCityCd();
        }
        return cityCd;
    }

    public String getVirtualCardNo() {
        if (null == virtualCardNo) {
            virtualCardNo = HookUtils.getVirtualCardNum();
        }
        return virtualCardNo;
    }

    public String getXTid() {
        if (null == xTid) {
            xTid = HookUtils.getHookXtid();
        }
        return xTid;
    }

    public String getSid() {
        if (null == sid) {
            sid = HookUtils.getHookSid();
        }
        return sid;
    }

    public String getUrid() {
        if (null == urid) {
            urid = HookUtils.getHookUrid(activity);
        }
        return urid;
    }

    public String getDfpSessionId() {
        if (null == sessionId) {
            sessionId = HookUtils.getHookDfpSessionId();
        }
        return sessionId;
    }

    public String getGray() {
        if (null == gray) {
            gray = HookUtils.getHookGray();
        }
        return gray;
    }

}
