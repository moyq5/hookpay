package com.hy.pay.hook.app.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.hy.pay.hook.app.iot.device.DeviceAction;
import com.hy.pay.hook.app.iot.device.data.NotifyData;

public class PayStatusReceiver extends BroadcastReceiver {

    private DeviceAction deviceAction;

    public PayStatusReceiver(DeviceAction deviceAction) {
        this.deviceAction = deviceAction;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        NotifyData data = new NotifyData();
        data.orderNo = intent.getStringExtra("orderNo");
        data.payType = intent.getStringExtra("payType");
        data.amount = intent.getStringExtra("amount");
        data.channelOrderNo = intent.getStringExtra("channelOrderNo");
        data.payAmount = intent.getStringExtra("payAmount");
        data.status = intent.getStringExtra("status");

        deviceAction.sendNotify(data);
    }
}
