package com.hy.pay.hook.app.impl.unionpay.hook;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;


import com.hy.pay.hook.app.HookLogger;

import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;

import de.robv.android.xposed.XposedHelpers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public abstract class HookUtils {
    private static final String TAG = HookUtils.class.getSimpleName();

    private static ClassLoader classLoader;

    public static void setClassLoader(ClassLoader classLoader) {
        HookUtils.classLoader = classLoader;
    }

    public static String getOrderList(int pageSize) {

        StringBuilder sb = new StringBuilder();
        sb.append("https://wallet.95516.com/app/inApp/order/list?currentPage=");
        sb.append(encrypt("1"));
        sb.append("&month=");
        sb.append(encrypt("0"));
        sb.append("&orderStatus=");
        sb.append(encrypt("0"));
        sb.append("&orderType=");
        sb.append(encrypt("A30000"));
        sb.append("&pageSize=");
        sb.append(encrypt(Integer.toString(pageSize)));

        final String queryString = sb.toString();

        HookLogger.d(TAG,"orderList->queryString: " + queryString);

        Unionpayer payer = Unionpayer.getInstance();

        Request req = new Request.Builder()
                .url(queryString)
                .header("X-Tingyun-Id", payer.getXTid())
                .header("X-Tingyun-Lib-Type-N-ST", "0;" + System.currentTimeMillis())
                .header("sid", payer.getSid())
                .header("urid", payer.getUrid())
                .header("cityCd", payer.getCityCd())
                .header("locale", "zh-CN")
                .header("User-Agent", "Android CHSP")
                .header("dfpSessionId", payer.getDfpSessionId())
                .header("gray", payer.getGray())
                .header("Accept", "*/*")
                .header("key_session_id", "")
                .header("Host", "wallet.95516.com")
                .build();


        String resBody;

        try {
            resBody = new OkHttpClient().newCall(req)
                    .execute().body().string();
        } catch (Exception e) {
            HookLogger.e(TAG, e);
            return null;
        }

        HookLogger.d(TAG, "orderList->resBody->encrypt: " + resBody);

        resBody = decrypt(resBody);

        HookLogger.d(TAG, "orderList->resBody->decrypt: " + resBody);

        return resBody;

    }

    public static String getHookXtid() {
        String xtid = "";
        try {
            Class clazz = XposedHelpers.findClass("com.networkbench.agent.impl.m.s", classLoader);

            String fObj = XposedHelpers.callMethod(XposedHelpers.callStaticMethod(clazz, "f", new Object[0]), "H", new Object[0]).toString();

            HookLogger.d(TAG, "getHookXtid->f: " + fObj);

            String iObj = XposedHelpers.callStaticMethod(clazz, "I", new Object[0]).toString();

            HookLogger.d(TAG, "getHookXtid->I: " + fObj);

            Method method = clazz.getDeclaredMethod("a", new Class[]{String.class, Integer.TYPE});

            xtid = method.invoke(null, new Object[]{fObj, Integer.parseInt(iObj)}).toString();

            HookLogger.d(TAG, "getHookXtid->xtid: " + xtid);

        } catch (Throwable e) {
            //HookLogger.e(TAG, e);
        }
        return xtid;
    }

    public static String getOrderInfo(String orderId) {

        String reqBody = "{\"orderType\":\"21\",\"transTp\":\"simple\",\"orderId\":\"" + orderId +"\"}";

        HookLogger.d(TAG, "getOrderInfo->reqBody: " + reqBody);

        Unionpayer payer = Unionpayer.getInstance();

        Request req = new Request.Builder()
                .url("https://wallet.95516.com/app/inApp/order/detail")
                .header("X-Tingyun-Id", payer.getXTid())
                .header("X-Tingyun-Lib-Type-N-ST", "0;" + System.currentTimeMillis())
                .header("sid", payer.getSid())
                .header("urid", payer.getUrid())
                .header("cityCd", payer.getCityCd())
                .header("locale", "zh-CN")
                .header("User-Agent", "Android CHSP")
                .header("dfpSessionId", payer.getDfpSessionId())
                .header("gray", payer.getGray())
                .header("Accept", "*/*")
                .header("key_session_id", "")
                .header("Content-Type", "application/json; charset=utf-8")
                .header("Host", "wallet.95516.com")
                .post(RequestBody.create(null, encrypt(reqBody)))
                .build();

        String resBody;

        try {
            resBody = new OkHttpClient().newCall(req)
                    .execute().body().string();
        } catch (Exception e) {
            HookLogger.e(TAG, e);
            return null;
        }

        HookLogger.v(TAG, "getOrderInfo->resBody->encrypt: " + resBody);

        resBody = decrypt(resBody);

        HookLogger.d(TAG, "getOrderInfo->resBody->decrypt: " + resBody);

        return resBody;

    }

    public static String getHookSid() {
        String sid = "";

        try {
            sid = XposedHelpers.callMethod(XposedHelpers.callStaticMethod(XposedHelpers.findClass("com.unionpay.network.aa", classLoader), "b", new Object[0]), "e", new Object[0]).toString();
        } catch (Throwable e) {
            HookLogger.e(TAG, e);
        }

        HookLogger.d(TAG, "getHookSid->sid: " + sid);

        return sid;
    }

    public static String getHookUrid(Activity activity) {
        String urid = "";
        try {
            Class clazz = XposedHelpers.findClass("com.unionpay.data.d", classLoader);
            Class[] classArr = new Class[1];
            classArr[0] = Context.class;
            Object[] objArr = new Object[1];
            objArr[0] = activity;
            String result = XposedHelpers.callMethod(XposedHelpers.callMethod(XposedHelpers.callStaticMethod(clazz, "a", classArr, objArr), "A", new Object[0]), "getHashUserId", new Object[0]).toString();
            if ((!TextUtils.isEmpty(result)) && (result.length() >= 15)) {
                urid = result.substring(result.length() - 15);
            }
        } catch (Throwable e) {
            HookLogger.e(TAG, e);
        }

        HookLogger.d(TAG, "getHookUrid->urid: " + urid);

        return urid;
    }

    public static String getHookCityCd() {
        String cityCd = "";

        try {
            cityCd = XposedHelpers.callStaticMethod(XposedHelpers.findClass("com.unionpay.location.a", classLoader), "i", new Object[0]).toString();
        } catch (Throwable e) {
            HookLogger.e(TAG, e);
        }

        HookLogger.d(TAG, "getHookCityCd->cityCd: " + cityCd);

        return cityCd;
    }

    public static String getHookDfpSessionId() {
        String sessionId = "";
        try {
            sessionId = XposedHelpers.callStaticMethod(XposedHelpers.findClass("com.unionpay.service.b", classLoader), "d", new Object[0]).toString();
        } catch (Throwable e) {
            HookLogger.e(TAG, e);
        }

        HookLogger.d(TAG, "getHookDfpSessionId->sessionId: " + sessionId);

        return sessionId;
    }

    public static String getHookGray() {
        String gray = "";
        try {
            gray = XposedHelpers.callMethod(XposedHelpers.callStaticMethod(XposedHelpers.findClass("com.unionpay.network.aa", classLoader), "b", new Object[0]), "d", new Object[0]).toString();
        } catch (Throwable e) {
            HookLogger.e(TAG, e);
        }

        HookLogger.d(TAG, "getHookGray->gray: " + gray);

        return gray;
    }

    public static String generateQrcode(String money, String remark) {
        Unionpayer payer = Unionpayer.getInstance();
        String amount = new BigDecimal(money).setScale(2, RoundingMode.HALF_UP).toPlainString().replace(".", "");

        StringBuilder sb = new StringBuilder();
        sb.append("https://pay.95516.com/pay-web/restlet/qr/p2pPay/applyQrCode?txnAmt=");
        sb.append(encrypt(amount));
        sb.append("&cityCode=");
        sb.append(encrypt(payer.getCityCd()));
        sb.append("&comments=");
        sb.append(encrypt(remark));
        sb.append("&virtualCardNo=");
        sb.append(payer.getVirtualCardNo());

        final String queryString = sb.toString();

        HookLogger.d(TAG, "generateQrcode->queryString: " + queryString);

        Request req = new Request.Builder()
                .url(queryString)
                .header("X-Tingyun-Id", payer.getXTid())
                .header("X-Tingyun-Lib-Type-N-ST", "0;" + System.currentTimeMillis())
                .header("sid", payer.getSid())
                .header("urid", payer.getUrid())
                .header("cityCd", payer.getCityCd())
                .header("locale", "zh-CN")
                .header("User-Agent", "Android CHSP")
                .header("dfpSessionId", payer.getDfpSessionId())
                .header("gray", payer.getGray())
                .header("key_session_id", "")
                .header("Host", "pay.95516.com")
                .build();

        String resBody;
        try {
            resBody = new OkHttpClient().newCall(req).execute().body().string();
        } catch (IOException e) {
            HookLogger.e(TAG, e);
            return null;
        }

        HookLogger.v(TAG, "generateQrcode->resBody->encrypt: " + resBody);

        resBody = decrypt(resBody);

        HookLogger.d(TAG, "generateQrcode->resBody->decrypt: " + resBody);

        return resBody;
    }

    public static String getVirtualCardNum() {
        Unionpayer payer = Unionpayer.getInstance();

        StringBuilder sb = new StringBuilder();

        sb.append("https://pay.95516.com/pay-web/restlet/qr/p2pPay/getInitInfo?cardNo=&cityCode=");
        sb.append(encrypt(payer.getCityCd()));

        final String queryString = sb.toString();

        HookLogger.d(TAG, "getVirtualCardNum->queryString: " + queryString);

        Request req = new Request.Builder()
                .url(queryString)
                .header("X-Tingyun-Id", payer.getXTid())
                .header("X-Tingyun-Lib-Type-N-ST", "0;" + System.currentTimeMillis())
                .header("sid", payer.getSid())
                .header("urid", payer.getUrid())
                .header("cityCd", payer.getCityCd())
                .header("locale", "zh-CN")
                .header("User-Agent", "Android CHSP")
                .header("dfpSessionId", payer.getDfpSessionId())
                .header("gray", payer.getGray())
                .header("key_session_id", "")
                .header("Host", "pay.95516.com")
                .build();

        String resBody;
        try {
            Response res = new OkHttpClient().newCall(req).execute();
            HookLogger.d(TAG, "getVirtualCardNum->response: " + res);
            resBody = res.body().string();
        } catch (Exception e) {
            HookLogger.e(TAG, e);
            return null;
        }

        HookLogger.v(TAG, "getVirtualCardNum->resBody->encrypt: " + resBody);

        resBody = decrypt(resBody);

        HookLogger.d(TAG, "getVirtualCardNum->resBody->decrypt: " + resBody);

        String virtualCardNo;

        try {
            virtualCardNo = new JSONObject(resBody).
                    getJSONObject("params").
                    getJSONArray("cardList").
                    getJSONObject(0).
                    getString("virtualCardNo");
        } catch (Exception e) {
            HookLogger.e(TAG, e);
            return null;
        }

        HookLogger.v(TAG, "getVirtualCardNum->virtualCardNo: " + virtualCardNo);

        virtualCardNo = encrypt(virtualCardNo);

        HookLogger.d(TAG, "getVirtualCardNum->virtualCardNo->encrypt: " + virtualCardNo);

        return virtualCardNo;
    }

    private static String encrypt(String str) {
        if (null == str || str.isEmpty()) {
            return "";
        }
        try {
            Class clazz = XposedHelpers.findClass("com.unionpay.encrypt.IJniInterface", classLoader);
            return (String) XposedHelpers.callStaticMethod(clazz, "encryptMsg", new Object[]{str});
        } catch (Throwable e) {
            HookLogger.e(TAG, "encrypt", e);
        }
        return "";
    }


    private static String decrypt(String str) {
        if (null == str || str.isEmpty()) {
            return "";
        }
        try {
            Class clazz = XposedHelpers.findClass("com.unionpay.encrypt.IJniInterface", classLoader);
            return (String) XposedHelpers.callStaticMethod(clazz, "decryptMsg", new Object[]{str});
        } catch (Throwable e) {
            HookLogger.e(TAG, "decrypt", e);
        }
        return "";
    }

}
