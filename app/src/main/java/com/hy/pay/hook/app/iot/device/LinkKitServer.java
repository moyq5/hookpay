package com.hy.pay.hook.app.iot.device;

import com.aliyun.alink.dm.api.DeviceInfo;
import com.aliyun.alink.linkkit.api.ILinkKitConnectListener;
import com.aliyun.alink.linkkit.api.IoTMqttClientConfig;
import com.aliyun.alink.linkkit.api.LinkKit;
import com.aliyun.alink.linkkit.api.LinkKitInitParams;
import com.aliyun.alink.linksdk.cmp.connect.channel.MqttSubscribeRequest;
import com.aliyun.alink.linksdk.cmp.core.listener.IConnectSubscribeListener;
import com.aliyun.alink.linksdk.tmp.device.payload.ValueWrapper;
import com.aliyun.alink.linksdk.tools.AError;
import com.hy.pay.hook.app.AppLogger;
import com.hy.pay.hook.app.Constants;

import java.util.HashMap;
import java.util.Map;

public class LinkKitServer implements DeviceService {
    private static final String TAG = LinkKitServer.class.getName();
    private static final DeviceService service = new LinkKitServer();
    private Device device;
    private Boolean isInited = false;

    public static DeviceService getInstance()  {
        return service;
    }

    public synchronized void start(Device device) throws DeviceStartedException {
        if (isInited) {
            throw new DeviceStartedException();
        }
        this.isInited = true;
        this.device = device;
        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.productKey = device.productKey;// 产品类型
        deviceInfo.deviceName = device.deviceName;// 设备名称
        deviceInfo.deviceSecret = device.deviceSecret;// 设备密钥

        Map<String, ValueWrapper> propertyValues = new HashMap<>();

        IoTMqttClientConfig clientConfig = new IoTMqttClientConfig(deviceInfo.productKey, deviceInfo.deviceName, deviceInfo.deviceSecret);

        LinkKitInitParams params = new LinkKitInitParams();
        params.deviceInfo = deviceInfo;
        params.propertyValues = propertyValues;
        params.mqttClientConfig = clientConfig;

        LinkKit.getInstance().registerOnPushListener(device.notifyListener);
        LinkKit.getInstance().init(device.context, params, createConnectListener());
    }

    @Override
    public Device device() {
        return device;
    }

    @Override
    public void stop() {
        LinkKit.getInstance().unRegisterOnPushListener(device.notifyListener);
        LinkKit.getInstance().deinit();
        isInited = false;
    }

    private ILinkKitConnectListener createConnectListener() {
       return new ILinkKitConnectListener() {
            @Override
            public void onError(AError error) {
                AppLogger.w(TAG, "初始化失败：" + error);
                retryInit();
            }
            @Override
            public void onInitDone(Object data) {
                AppLogger.i(TAG, "初始化成功");
                initSubscribes();
            }
        };
    }

    private void retryInit() {
        stop();
        isInited = false;
        new Thread(() -> {
            try {
                AppLogger.i(TAG, "初始化10秒后重试");

                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                AppLogger.i(TAG, "初始化重试");
                start(device);
            } catch (DeviceStartedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private void initSubscribes() {
        final MqttSubscribeRequest orderSubscribe = new MqttSubscribeRequest();
        orderSubscribe.topic = String.format(Constants.TOPIC_ORDER_FORMAT, device.productKey, device.deviceName);
        orderSubscribe.isSubscribe = true;
        LinkKit.getInstance().subscribe(orderSubscribe, new IConnectSubscribeListener() {
            @Override
            public void onSuccess() {
                // 订阅成功
                AppLogger.d(TAG, "订阅：" + orderSubscribe.topic);
            }

            @Override
            public void onFailure(AError aError) {
                // 订阅失败
                AppLogger.w(TAG, "订阅失败：" + orderSubscribe.topic);
            }
        });
        final MqttSubscribeRequest replySubscribe = new MqttSubscribeRequest();
        replySubscribe.topic = String.format(Constants.TOPIC_NOTIFY_REPLY_FORMAT, device.productKey, device.deviceName);
        replySubscribe.isSubscribe = true;
        LinkKit.getInstance().subscribe(replySubscribe, new IConnectSubscribeListener() {
            @Override
            public void onSuccess() {
                // 订阅成功
                AppLogger.d(TAG, "订阅：" + replySubscribe.topic);
            }

            @Override
            public void onFailure(AError aError) {
                // 订阅失败
                AppLogger.w(TAG, "订阅失败：" + replySubscribe.topic);
            }
        });

    }
}
