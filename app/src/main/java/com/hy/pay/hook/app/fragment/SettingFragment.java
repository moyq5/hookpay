package com.hy.pay.hook.app.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hy.pay.hook.app.Main;
import com.hy.pay.hook.app.R;
import com.hy.pay.hook.app.ScanActivity;
import com.hy.pay.hook.app.impl.quanfutong.QuanfutongMain;
import com.hy.pay.hook.app.impl.unionpay.UnionpayMain;
import com.hy.pay.hook.app.iot.device.ConnectNotifyListener;
import com.hy.pay.hook.app.iot.device.Device;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import okhttp3.internal.DiskLruCache;
import okhttp3.internal.io.FileSystem;
import okio.Buffer;
import okio.Sink;
import okio.Source;

import static android.os.Environment.MEDIA_MOUNTED;
import static android.os.Environment.getExternalStorageState;
import static android.os.Environment.isExternalStorageRemovable;

public class SettingFragment extends Fragment {

    private Main main;
    private Boolean isMainCreated = false;
    private Integer hookType = 0;
    private EditText nameText;
    private EditText productKeyText;
    private EditText deviceNameText;
    private EditText deviceSecretText;
    private Button startBtn;
    private Button scanBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);

        nameText = view.findViewById(R.id.name);
        productKeyText = view.findViewById(R.id.productKey);
        deviceNameText = view.findViewById(R.id.deviceName);
        deviceSecretText = view.findViewById(R.id.deviceSecret);
        scanBtn = view.findViewById(R.id.scanBtn);
        scanBtn.setOnClickListener(createScanBtnClickListner());
        startBtn = view.findViewById(R.id.startBtn);
        startBtn.setOnClickListener(createStartBtnClickListner());

        Bundle bundle;
        String data;
        if (null != (bundle = getArguments()) && null != (data = bundle.getString("settingData")) ) {

        } else if(null != (data = readSettingData())) {

        }

        if (null != data) {
            String[] vars = data.split("\\|", -1);
            hookType = Integer.parseInt(vars[0]);
            nameText.setText(vars[1]);
            productKeyText.setText(vars[2]);
            deviceNameText.setText(vars[3]);
            deviceSecretText.setText(vars[4]);
        }

        return view;
    }

    private View.OnClickListener createScanBtnClickListner() {
        return view -> startActivity(new Intent(this.getContext(), ScanActivity.class));
    }

    private View.OnClickListener createStartBtnClickListner() {
        return view -> {
            if (isMainCreated) {
                main.destroy();
                nameText.setEnabled(true);
                productKeyText.setEnabled(true);
                deviceNameText.setEnabled(true);
                deviceSecretText.setEnabled(true);
                scanBtn.setEnabled(true);
                startBtn.setText("开始");
            } else {
                nameText.setEnabled(false);
                productKeyText.setEnabled(false);
                deviceNameText.setEnabled(false);
                deviceSecretText.setEnabled(false);
                scanBtn.setEnabled(false);
                startBtn.setText("停止");

                writeSettingData();


                View rootView = getView().getRootView();
                ViewPager pager = rootView.findViewById(R.id.frag_pager);
                pager.setCurrentItem(1,true);
                ((TextView)pager.findViewById(R.id.logConsoleView)).setText("(空)");

                Device device = new Device();
                device.context = getContext();
                device.deviceName = deviceNameText.getText().toString();
                device.deviceSecret = deviceSecretText.getText().toString();
                device.productKey = productKeyText.getText().toString();
                device.notifyListener = new ConnectNotifyListener(device);

                if (hookType == 0) {
                    getActivity().setTitle("全付通-" + nameText.getText().toString());
                    main = new QuanfutongMain(device);

                } else if (hookType == 1) {
                    getActivity().setTitle("云闪付-" + nameText.getText().toString());
                    main = new UnionpayMain(device);
                } else {
                    Toast.makeText(getContext(),"not support init type: " + hookType, Toast.LENGTH_SHORT);
                    return;
                }

                main.create();
            }
            isMainCreated = !isMainCreated;

        };
    }

    @Override
    public void onDestroy() {
        if (null != main) {
            main.destroy();
        }
        super.onDestroy();
    }

    private String readSettingData() {
        String data = null;
        try (DiskLruCache cache = DiskLruCache.create(FileSystem.SYSTEM, getCacheFile(), 0, 1, 300)){
            DiskLruCache.Snapshot snapshot = cache.get("setting");
            if (null != snapshot) {
                try (Source source = snapshot.getSource(0); Buffer buffer = new Buffer()) {
                    source.read(buffer, 300);
                    data = buffer.readUtf8();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return (null == data || data.isEmpty()) ? null: data;
    }
    private void writeSettingData() {
        final String data = String.format("%d|%s|%s|%s|%s",
                hookType,
                nameText.getText().toString(),
                productKeyText.getText().toString(),
                deviceNameText.getText().toString(),
                deviceSecretText.getText().toString());

        try (DiskLruCache cache = DiskLruCache.create(FileSystem.SYSTEM, getCacheFile(), 0, 1, 300)){
            DiskLruCache.Editor editor = cache.edit("setting");
            try (Sink sink = editor.newSink(0);
                 Buffer buffer = new Buffer();
                 OutputStream os =  buffer.outputStream()) {
                os.write(data.getBytes("UTF-8"));
                sink.write(buffer, buffer.size());
                sink.flush();
            }
            editor.commit();
            cache.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File getCacheFile() {
        final String cachePath = MEDIA_MOUNTED.equals(getExternalStorageState())
                ||!isExternalStorageRemovable()
                ? getContext().getExternalCacheDir().getPath()
                : getContext().getCacheDir().getPath();
        File file = new File(cachePath + File.separator + "setting");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }
}
